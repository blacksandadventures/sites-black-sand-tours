const theme = {
  palette: {
    text: {
      primary: 'rgba(68, 68, 68, 1)',
      secondary: 'rgba(255, 255, 255, 1)',
      grey: 'rgba(102, 102, 102, 1)',
      lightGrey: 'rgba(204, 204, 204, 1)',
      black: 'rgba(0, 0, 0, 1)',
      primaryBlend: 'rgb(40, 93, 95)'
    },
    border: {
      dark: 'rgba(204, 204, 204, 1)',
      light: 'rgba(255, 255, 255, 0.78)'
    },
    background: {
      dark: 'rgba(34, 34, 34, 1)',
      middle: 'rgba(241, 241, 238, 1)',
      light: 'rgba(255, 255, 255, 1)',
      overlay: 'rgba(0, 0, 0, 0.25)',
      modal: 'rgba(0, 0, 0, 0.45)'
    },
    primary: {
      main: 'rgba(85, 187, 192, 1)',
      hover: 'rgba(69, 152, 156, 1)',
      overlay: 'rgba(69, 152, 156, 0.8)'
    },
    secondary: {
      main: 'rgba(34, 34, 34, 1)',
      hover: 'rgba(15, 19, 25, 1)'
    },
    alert: {
      danger: {
        main: '#f44336',
        hover: '#9a2820'
      }
    }
  },
  responsive: {
    small: '48em',
    medium: '74em'
  },
  font: {
    body: `'Comfortaa', cursive`,
    header: `'Nunito', san-serif`,
    cursive: `'Caveat', cursive`
  }
};

export default theme;
