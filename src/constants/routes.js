export const INDEX = '/';

// Tour Pages
export const WHAT_WE_DO = '/what-we-do/';
export const WHERE_WE_GO = '/where-we-go/';
export const REVIEWS = '/reviews/';
export const FAQ = '/frequently-asked-questions/';

// Company Pages
export const IMAGE_GALLERY = '/image-gallery/';
export const ABOUT_US = '/about-us/';
export const PICKUP_LOCATIONS = '/pickup-locations/';
export const CONTACT = '/contact-us/';

// Thank you Pages
export const THANKYOU = '/thankyou/';
export const THANK_YOU = '/thank-you/';
