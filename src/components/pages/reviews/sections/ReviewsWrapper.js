import styled from 'styled-components';

const ReviewsWrapper = styled.div`
  position: relative;
  margin-top: -15px;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    margin-top: 0;
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default ReviewsWrapper;
