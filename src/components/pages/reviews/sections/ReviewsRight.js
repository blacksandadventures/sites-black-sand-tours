import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 40px 20px 40px 0;
  width: 50%;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin: 0;
    width: 100%;
    text-align: center;
  }
`;

const ReviewSection = styled.div`
  li {
    margin: 30px 0;
  }
  h3 {
    color: ${props => props.theme.palette.text.black};
  }
  h6 {
    color: ${props => props.theme.palette.text.black};
    text-transform: uppercase;
    margin-bottom: 5px;
  }
  img {
    height: 15px;
  }
  p {
    margin: 0;
  }
`;

const ReviewsRight = props => {
  const { tripAdvisorData } = props;
  return (
    <Wrapper>
      <ReviewSection>
        <h3>Most Recent Traveller Reviews</h3>
        <ul>
          {tripAdvisorData.reviews.map(i => {
            const truncateLength = 98;
            const reviewText = i.text.substring(0, truncateLength);
            return (
              <li key={i.id}>
                <h6>{i.title}</h6>
                <img
                  src={i.rating_image_url.replace(/^http:\/\//i, 'https://')}
                  alt={i.text}
                />
                <p>
                  {reviewText}
                  ...&nbsp;
                  <a
                    href={i.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    title={`Read more ${i.text}`}
                  >
                    Read more
                  </a>
                </p>
              </li>
            );
          })}
        </ul>
      </ReviewSection>
    </Wrapper>
  );
};

export default ReviewsRight;
