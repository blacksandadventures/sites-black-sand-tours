import React, { Component, Fragment } from 'react';

import {
  ReviewsWrapper,
  ReviewsInner,
  ReviewsLeft,
  ReviewsRight
} from './sections';
import { Loading } from '../../global/templates';

class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reviewData: [],
      isLoading: true
    };
  }

  componentDidMount() {
    const { tripAdvisorId } = this.props;
    const TA_KEY = '2380ee0b7c304f6eb06625ae38184c8b';
    const self = this;
    const proxyurl = 'https://cors-anywhere.herokuapp.com/';
    const url = `//api.tripadvisor.com/api/partner/2.0/location/${tripAdvisorId}?key=${TA_KEY}`;
    fetch(proxyurl + url)
      .then(data => data.json())
      .then(data => {
        self.setState({
          reviewData: data,
          isLoading: false
        });
      });
  }

  render() {
    const { isLoading, reviewData } = this.state;
    return (
      <ReviewsWrapper>
        <ReviewsInner>
          {isLoading && <Loading />}
          {!isLoading && (
            <Fragment>
              <ReviewsLeft tripAdvisorData={reviewData} />
              <ReviewsRight tripAdvisorData={reviewData} />
            </Fragment>
          )}
        </ReviewsInner>
      </ReviewsWrapper>
    );
  }
}

export default Reviews;
