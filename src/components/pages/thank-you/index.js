import React, { Component } from 'react';

import {
  ThanksInner,
  Boxes,
  FirstBox,
  SecondBox,
  ThirdBox,
  ThanksTitle
} from './sections';

class Thanks extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ThanksInner>
        <ThanksTitle />
        <Boxes>
          <FirstBox />
          <SecondBox />
          <ThirdBox />
        </Boxes>
      </ThanksInner>
    );
  }
}

export default Thanks;
