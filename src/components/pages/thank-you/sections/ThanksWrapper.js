import styled from 'styled-components';

const ThanksWrapper = styled.div`
  position: fixed;
  background: ${props => props.theme.palette.primary.main};
  min-height: 100vh;
  padding-bottom: 100px;
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default ThanksWrapper;
