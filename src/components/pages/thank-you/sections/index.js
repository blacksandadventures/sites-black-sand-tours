import ThanksWrapper from './ThanksWrapper';
import ThanksInner from './ThanksInner';
import ThanksTitle from './ThanksTitle';
import Boxes from './Boxes';
import FirstBox from './FirstBox';
import MailChimp from './MailChimp';
import SecondBox from './SecondBox';
import ThirdBox from './ThirdBox';

export {
  ThanksWrapper,
  ThanksInner,
  ThanksTitle,
  Boxes,
  FirstBox,
  MailChimp,
  SecondBox,
  ThirdBox
};
