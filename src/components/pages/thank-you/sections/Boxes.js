import styled from 'styled-components';

const Boxes = styled.div`
  position: relative;
  display: flex;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    flex-direction: column;
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default Boxes;
