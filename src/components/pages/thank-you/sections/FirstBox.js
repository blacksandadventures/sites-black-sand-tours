import React from 'react';
import styled from 'styled-components';
import { MailChimp } from '.';

const Wrapper = styled.div`
  padding-left: 30px;
  position: relative;
  padding-bottom: 40px;
  text-align: left;
`;

const Inner = styled.div`
  width: 91.67%;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    width: 100%;
  }
`;

const Layout = styled.div`
  list-style: none;
  margin-left: -20px;
  padding: 0px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-left: -20px;
  }
  &:after {
    z-index: 10;
    position: absolute;
    content: '';
    height: 2px;
    background-color: rgb(204, 204, 204);
    top: 14px;
    width: 100%;
    left: 30px;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      left: 35px;
      top: 14px;
      width: 2px;
      height: 100%;
    }
  }
`;

const Container = styled.div`
  display: inline-block;
  padding-left: 24px;
  vertical-align: top;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    padding-left: 20px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-left: 10px;
    &:first-child {
      width: 15%;
    }
    &:last-child {
      width: 85%;
    }
  }
`;

const Step = styled.div`
  margin-bottom: 12px;
`;

const Number = styled.div`
  line-height: 30px;
  background-color: rgb(255, 255, 255);
  color: rgb(204, 204, 204);
  height: 32px;
  position: relative;
  text-align: center;
  top: -2px;
  width: 32px;
  z-index: 20;
  border-color: rgb(204, 204, 204);
  border-style: solid;
  border-radius: 50%;
  border-width: 2px;
`;

const FirstBox = () => (
  <Wrapper>
    <Inner>
      <Layout>
        <Container>
          <Step>
            <Number>1</Number>
          </Step>
        </Container>
        <Container>
          <h4>Where should we send your photos?</h4>
          <p>
            It&apos;s likely, we have many photos of your adventure so leave
            your email and we&apos;ll send a link.
          </p>
          <MailChimp />
        </Container>
      </Layout>
    </Inner>
  </Wrapper>
);

export default FirstBox;
