import React from 'react';
import styled from 'styled-components';
import { TripAdvisorIcon } from '../../../../assets/images/siteIcons';

const Wrapper = styled.div`
  padding-left: 30px;
  position: relative;
  padding-bottom: 40px;
  text-align: left;
`;

const Inner = styled.div`
  width: 91.67%;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    width: 100%;
  }
`;

const Layout = styled.div`
  list-style: none;
  margin-left: -20px;
  padding: 0px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-left: -20px;
  }
`;

const Container = styled.div`
  display: inline-block;
  padding-left: 24px;
  vertical-align: top;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    padding-left: 20px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-left: 10px;
    &:first-child {
      width: 15%;
    }
    &:last-child {
      width: 85%;
    }
  }
  a {
    width: 100%;
    outline: none;
    background: #4da681;
    color: ${props => props.theme.palette.text.secondary};
    border: none;
    margin-top: 15px;
    padding: 14px;
    font-size: 14px;
    font-weight: 700;
    line-height: 20px;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    &:hover {
      color: #fff;
      opacity: 0.9;
    }
    svg {
      margin-left: 15px;
      width: 35px;
      height: 25px;
      path {
        fill: #fff;
      }
    }
  }
`;

const Step = styled.div`
  margin-bottom: 12px;
`;

const Number = styled.div`
  line-height: 30px;
  background-color: rgb(255, 255, 255);
  color: rgb(204, 204, 204);
  height: 32px;
  position: relative;
  text-align: center;
  top: -2px;
  width: 32px;
  z-index: 20;
  border-color: rgb(204, 204, 204);
  border-style: solid;
  border-radius: 50%;
  border-width: 2px;
`;

const ThirdBox = () => (
  <Wrapper>
    <Inner>
      <Layout>
        <Container>
          <Step>
            <Number>3</Number>
          </Step>
        </Container>
        <Container>
          <h4>Would you like to leave us a review?</h4>
          <p>
            Thank you for choosing Black Sand Adventures. We would be extremely
            grateful if you left us a review.
          </p>
          <a
            href="https://www.tripadvisor.com/UserReview-g255106-d10467767-m27451-Black_Sand_Tours-Auckland_Central_North_Island.html"
            target="_blank"
            rel="noopener noreferrer"
            title="Write a review for Black Sand Tours"
          >
            Write Review <TripAdvisorIcon />
          </a>
        </Container>
      </Layout>
    </Inner>
  </Wrapper>
);

export default ThirdBox;
