import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding: 30px;
  position: relative;
  text-align: left;
  line-height: 1.75;
  h2 {
    margin-bottom: 10px;
    text-align: left;
  }
  p {
    margin-top: 0px;
  }
`;

const ThanksTitle = () => (
  <Wrapper>
    <h2>Thank you for a great day!</h2>
    <p>Don&apos;t forget to share and review your experience.</p>
  </Wrapper>
);

export default ThanksTitle;
