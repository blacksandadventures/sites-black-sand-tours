import React from 'react';
import styled from 'styled-components';
import MailchimpSubscribe from 'react-mailchimp-subscribe';

const Wrapper = styled.div`
  position: relative;
  height: calc(100% - 43px);
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    height: calc(100% - 40px);
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    height: 100%;
    padding: 20px 0;
  }
  &:before,
  &:after {
    content: '';
    z-index: 2;
    border-width: 2px;
    border-style: solid;
    border-color: ${props => props.theme.palette.primary.main};
    border-image: initial;
  }
  &:before {
    position: absolute;
    top: 0px;
    right: 10px;
    bottom: 10px;
    left: 0px;
  }
  &:after {
    position: absolute;
    top: 10px;
    right: 0px;
    bottom: 0px;
    left: 10px;
  }
`;

const Inner = styled.div`
  transform: translate3d(0px, -50%, 0px);
  position: relative;
  z-index: 5;
  top: 50%;
  padding: 2em;
  h4 {
    text-align: center;
    text-transform: uppercase;
    font-size: 1.25em;
    color: ${props => props.theme.palette.text.black};
    letter-spacing: 0.25em;
  }
  > div {
    display: flex;
    flex-direction: column;
    justify-content: center;
    text-align: center;
    > div {
      &:first-child {
        font-size: 80%;
        margin: 10px 0;
        a {
          margin: 10px 0 0;
        }
      }
    }
    input {
      outline: none;
      font-family: ${props => props.theme.font.header};
      background: ${props => props.theme.palette.background.light};
      box-sizing: border-box;
      width: 100%;
      border: 1px solid ${props => props.theme.palette.border.dark};
      font-size: 14px;
      line-height: 20px;
      letter-spacing: 1px;
      color: ${props => props.theme.palette.text.primary};
      padding: 14px;
      resize: none;
      -webkit-appearance: none;
      border-radius: 0;
      transition: all 300ms ease-in-out;
      &:hover,
      &:focus {
        border: 1px solid ${props => props.theme.palette.primary.main};
      }
    }
    label {
      font-family: ${props => props.theme.font.header};
      color: ${props => props.theme.palette.text.grey};
      font-size: 14px;
      opacity: 0;
      visibility: hidden;
      height: 0;
      width: 0;
      position: absolute;
      z-index: -1;
    }
    button {
      cursor: pointer;
      width: 70px;
      margin: 0 auto;
      outline: none;
      border: 0;
      background: 0;
      padding: 0 0 0.6em;
      position: relative;
      display: inline-block;
      font-family: ${props => props.theme.font.header};
      line-height: 1.2em;
      text-transform: uppercase;
      font-size: 0.85em;
      color: ${props => props.theme.palette.text.black};
      letter-spacing: 0.25em;
      transition: all 300ms ease-in-out;
      &:after {
        content: '';
        position: absolute;
        right: 0px;
        bottom: 0px;
        left: 0px;
        height: 1px;
        background-color: ${props => props.theme.palette.text.black};
      }
      &:hover {
        color: ${props => props.theme.palette.primary.main};
      }
      @media screen and (max-width: ${props => props.theme.responsive.medium}) {
        width: 57px;
      }
    }
  }
`;

const CustomForm = ({ status, message, onValidated }) => {
  let email;
  const submit = () =>
    email &&
    email.value.indexOf('@') > -1 &&
    onValidated({
      EMAIL: email.value
    });

  return (
    <div>
      {status === 'sending' && <div style={{ color: 'blue' }}>sending...</div>}
      {status === 'error' && (
        <div
          style={{ color: 'red' }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
      {status === 'success' && (
        <div
          style={{ color: 'green' }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
      <label htmlFor="email">Email</label>
      <input
        id="email"
        name="email"
        ref={node => (email = node)}
        type="email"
        placeholder="Your email"
      />
      <br />
      <button onClick={submit} type="button">
        Submit
      </button>
    </div>
  );
};

const url =
  'https://blacksandtours.us12.list-manage.com/subscribe/post?u=9ba7c2a53ea0d449d14eba316&amp;id=c7273bf25f';

const MailChimp = () => (
  <Wrapper>
    <Inner>
      <h4>Newsletter</h4>
      <MailchimpSubscribe
        url={url}
        render={({ subscribe, status, message }) => (
          <CustomForm
            status={status}
            message={message}
            onValidated={formData => subscribe(formData)}
          />
        )}
      />
    </Inner>
  </Wrapper>
);

export default MailChimp;
