import NewsletterTitle from './NewsletterTitle';
import NewsletterBox from './NewsletterBox';
import MailChimp from './MailChimp';
import SocialWrapper from './SocialWrapper';
import SocialLinks from './SocialLinks';
import HomeModal from './HomeModal';
import ModalSlider from './ModalSlider';
import ModalShare from './ModalShare';
import ReviewsTitle from './ReviewsTitle';

export {
  NewsletterTitle,
  NewsletterBox,
  MailChimp,
  SocialWrapper,
  SocialLinks,
  HomeModal,
  ModalSlider,
  ModalShare,
  ReviewsTitle
};
