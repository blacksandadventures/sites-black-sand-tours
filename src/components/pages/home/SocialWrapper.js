import React, { Component, Fragment } from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import styled from 'styled-components';

import * as ROUTES from '../../../constants/routes';
import { SocialLinks, HomeModal } from '.';

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr minmax(auto, 55%);
  grid-gap: 30px;
  align-items: center;
  z-index: 4;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    grid-template-columns: 1fr;
  }
`;

const Left = styled.div`
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    padding-top: 30px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    text-align: center;
    margin-bottom: 35px;
  }
  h2 {
    color: ${props => props.theme.palette.text.secondary};
  }
  h4 {
    color: ${props => props.theme.palette.text.secondary};
    &:not(:first-child) {
      margin-top: 100px;
      margin-bottom: 0px;
      @media screen and (max-width: ${props => props.theme.responsive.small}) {
        margin-top: 35px;
      }
    }
  }
  p {
    a {
      font-family: ${props => props.theme.font.header};
      line-height: 1.2em;
      text-transform: uppercase;
      display: inline-block;
      position: relative;
      font-size: 90%;
      color: ${props => props.theme.palette.text.secondary};
      letter-spacing: 0.25em;
      padding-bottom: 0.6em;
      &:hover {
        color: ${props => props.theme.palette.primary.main};
      }
      &:after {
        content: '';
        position: absolute;
        right: 0px;
        bottom: 0px;
        left: 0px;
        height: 1px;
        background-color: ${props => props.theme.palette.text.secondary};
      }
    }
  }
`;

const Right = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 2px;
  position: relative;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    grid-template-columns: 1fr 1fr;
  }
  button {
    border: 0;
    border: none;
    padding: 0;
    margin: 0;
    background: none;
    outline: none;
    min-height: 200px;
    position: relative;
    overflow: hidden;
    cursor: pointer;
    &:nth-child(n + 4) {
      max-width: 100%;
      margin-left: -60px;
    }
    &:nth-child(n + 7) {
      display: none;
    }
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      min-height: 250px;
      max-width: 100%;
      &:nth-child(n + 3) {
        margin-left: -60px;
      }
      &:nth-child(n + 5) {
        display: none;
      }
    }
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      min-height: 175px;
      &:nth-child(n + 3) {
        margin-left: 0;
      }
      &:nth-child(n + 5) {
        display: none;
      }
    }
  }
  .gatsby-image-wrapper {
    position: absolute !important;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    img {
      transform: scale(1);
      transition: all 1s cubic-bezier(0.59, 0, 0.06, 1) 0s !important;
      &:hover {
        transform: scale(1.1);
      }
    }
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

class SocialWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeImage: null,
      openModal: false
    };
  }

  handleModalOpen = event => {
    const imageindex = event.currentTarget.dataset.imageid;
    this.setState({
      activeImage: imageindex,
      openModal: true
    });
  };

  handleModalClose = () => {
    this.setState({
      openModal: false,
      activeImage: null
    });
  };

  render() {
    const { contentSection } = this.props;
    const { openModal, activeImage } = this.state;

    return (
      <StaticQuery
        query={graphql`
          query {
            contentfulCompanyInformation(
              id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
            ) {
              email
              phone
              rezdyBookingLink
            }
          }
        `}
        render={data => {
          const companyInfo = data.contentfulCompanyInformation;
          return (
            <Wrapper>
              <Left>
                <h4>{contentSection.name}</h4>
                <h2>
                  <Link
                    title="Black Sand Tours social media"
                    to={ROUTES.IMAGE_GALLERY}
                  >
                    #TRAVELLIKEALOCAL
                  </Link>
                </h2>
                <p>
                  <Link
                    title="Black Sand Tours image gallery"
                    to={ROUTES.IMAGE_GALLERY}
                  >
                    View Our Image Gallery
                  </Link>
                </p>
                <h4>Stay in touch with us:</h4>
                <SocialLinks />
              </Left>
              <Right>
                <Fragment>
                  {contentSection.imageGallery.map((i, index) => (
                    <button
                      key={i.id}
                      type="button"
                      data-imageid={index}
                      onClick={this.handleModalOpen}
                    >
                      <Img
                        fluid={i.fluid}
                        title={i.title}
                        alt={i.description}
                      />
                    </button>
                  ))}
                  {openModal && activeImage !== null && (
                    <HomeModal
                      showModal={openModal}
                      closeModal={this.handleModalClose}
                      images={contentSection.imageGallery}
                      activeImage={activeImage}
                      activeGallery="Image"
                      companyInfo={companyInfo}
                    />
                  )}
                </Fragment>
              </Right>
            </Wrapper>
          );
        }}
      />
    );
  }
}

export default SocialWrapper;
