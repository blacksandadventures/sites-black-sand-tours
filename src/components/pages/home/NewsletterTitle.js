import React from 'react';
import styled from 'styled-components';
import { StaticQuery, graphql } from 'gatsby';

const Wrapper = styled.div`
  text-align: center;
  position: relative;
  margin: 100px 0 30px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin: 30px 0;
    position: relative;
  }
`;

const Availability = styled.a`
  font-family: ${props => props.theme.font.header};
  line-height: 1.2em;
  text-transform: uppercase;
  font-size: 75%;
  letter-spacing: 0.25em;
  color: ${props => props.theme.palette.text.black};
  padding: 0.75em 0;
  &:after {
    content: '';
    position: absolute;
    right: 0px;
    bottom: 0px;
    left: 0px;
    height: 1px;
    transition: all 300ms ease-in-out;
    background-color: ${props => props.theme.palette.text.black};
  }
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    position: absolute;
    bottom: 7px;
    right: 0;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-bottom: 35px;
    position: relative;
  }
  &:hover {
    color: ${props => props.theme.palette.text.grey};
    &:after {
      background-color: ${props => props.theme.palette.text.grey};
    }
  }
`;

const HashTag = styled.h5`
  text-align: center;
`;

const Title = styled.h2`
  text-align: center;
`;

const NewsletterTitle = () => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          rezdyBookingLink
        }
      }
    `}
    render={data => {
      const { rezdyBookingLink } = data.contentfulCompanyInformation;
      return (
        <Wrapper>
          <HashTag>#travellikealocal</HashTag>
          <Title>Made for anyone</Title>
          <Availability
            href={rezdyBookingLink}
            target="_blank"
            rel="noopener noreferrer"
            title="Book your Black Sand Adventures Tour"
          >
            Check tour availability
          </Availability>
        </Wrapper>
      );
    }}
  />
);

export default NewsletterTitle;
