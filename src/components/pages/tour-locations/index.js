import React, { Fragment, Component } from 'react';
import { StaticQuery, graphql } from 'gatsby';

import {
  Header,
  Container,
  GalleryDesktop,
  SliderMobile,
  Details
} from './sections';

class TourLocations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageindex: null,
      showModal: false
    };
  }

  handleModalOpen = event => {
    const imageindex = event.currentTarget.dataset.imageid;
    this.setState({
      showModal: true,
      imageindex
    });
  };

  handleModalClose = () => {
    this.setState({
      showModal: false,
      imageindex: null
    });
  };

  render() {
    const { imageindex, showModal } = this.state;
    return (
      <StaticQuery
        query={graphql`
          query {
            contentfulCompanyInformation(
              id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
            ) {
              email
              phone
              rezdyBookingLink
              google
            }
          }
        `}
        render={data => {
          const companyInfo = data.contentfulCompanyInformation;
          const { locationInfo, location, pageContext } = this.props;
          return (
            <Fragment>
              <Header pageContext={pageContext} />
              <Container>
                <GalleryDesktop
                  locationInfo={locationInfo}
                  companyInfo={companyInfo}
                  location={location}
                  imageindex={imageindex}
                  showModal={showModal}
                  openModal={this.handleModalOpen}
                  closeModal={this.handleModalClose}
                />
                <SliderMobile locationInfo={locationInfo} />
                <Details
                  location={location}
                  locationInfo={locationInfo}
                  companyInfo={companyInfo}
                />
              </Container>
            </Fragment>
          );
        }}
      />
    );
  }
}

export default TourLocations;
