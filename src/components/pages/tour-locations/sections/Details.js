import React from 'react';
import styled from 'styled-components';
import { DetailsShare } from '.';

const Wrapper = styled.div`
  width: 35%;
  height: calc(100vh - 63px);
  padding: 4em;
  min-width: 0px;
  min-height: 0px;
  overflow: scroll;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    display: block;
    width: 100%;
    height: auto;
  }
  h1 {
    font-size: 2.5em;
    line-height: 1.15;
  }
`;

const ExternalGalleryLink = styled.div`
  display: flex;
  justify-content: left;
  a {
    color: ${props => props.theme.palette.text.secondary};
    font-family: ${props => props.theme.font.header};
    font-size: 0.75em;
    line-height: 1.2em;
    text-transform: uppercase;
    display: inline-block;
    position: relative;
    letter-spacing: 0.25em;
    background: ${props => props.theme.palette.primary.main};
    padding: 2em 2.5em;
    margin: 30px 0 0;
    &:hover {
      color: ${props => props.theme.palette.text.secondary};
      background: ${props => props.theme.palette.primary.hover};
    }
  }
`;

const Details = props => {
  const { locationInfo, companyInfo, location } = props;
  return (
    <Wrapper>
      <DetailsShare
        locationInfo={locationInfo}
        companyInfo={companyInfo}
        location={location}
      />
      <h1>{locationInfo.seoContent.pageTitle}</h1>
      <div
        dangerouslySetInnerHTML={{
          __html: locationInfo.body.childMarkdownRemark.html
        }}
      />
      <ExternalGalleryLink>
        <a
          href={companyInfo.rezdyBookingLink}
          target="_blank"
          rel="noopener noreferrer"
          title={`Book Black Sand tour to ${locationInfo.seoContent.pageTitle}`}
        >
          Book your adventure
        </a>
      </ExternalGalleryLink>
    </Wrapper>
  );
};

export default Details;
