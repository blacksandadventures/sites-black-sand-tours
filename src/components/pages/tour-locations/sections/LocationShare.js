import React from 'react';
import styled from 'styled-components';
import {
  FacebookShareButton,
  TwitterShareButton,
  PinterestShareButton,
  EmailShareButton,
  FacebookIcon,
  TwitterIcon,
  PinterestIcon,
  EmailIcon
} from 'react-share';
import config from '../../../../utils/siteConfig';

const Wrapper = styled.div`
  position: absolute;
  top: 45px;
  right: 136px;
  width: 46px;
  background: ${props => props.theme.palette.primary.main};
  z-index: 100;
  border: 1px solid rgba(0, 0, 0, 0.3);
  border-top: 0;
`;

const ShareLinks = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  height: auto;
  > div {
    cursor: pointer;
    width: 100%;
    height: 44px;
    border-top: 1px solid rgba(0, 0, 0, 0.3);
    display: flex;
    justify-content: center;
    align-items: center;
    &:first-child {
      border-top: 0;
    }
    svg {
      height: 40px !important;
      width: 40px;
      rect {
        fill: transparent;
      }
      path {
        transition: fill 0.2s ease-in-out;
      }
    }
    &:hover {
      background: ${props => props.theme.palette.primary.hover};
    }
  }
`;

const ShareIcons = props => {
  const { locationInfo, location } = props;
  const shareURl = config.siteUrl + location.pathname;
  const media = locationInfo.seoContent.featuredImage.fluid.src;
  const pageTitle = locationInfo.seoContent.pageTitle + '-' + config.siteTitle;
  return (
    <Wrapper className="shareButtons">
      <ShareLinks>
        <FacebookShareButton url={shareURl} quote={pageTitle}>
          <FacebookIcon size={40} />
        </FacebookShareButton>
        <TwitterShareButton url={shareURl}>
          <TwitterIcon size={40} />
        </TwitterShareButton>
        <PinterestShareButton url={shareURl} media={media}>
          <PinterestIcon size={40} />
        </PinterestShareButton>
        <EmailShareButton
          url={shareURl}
          subject={`Thought you may like this by ${pageTitle}`}
          body={shareURl}
        >
          <EmailIcon size={40} />
        </EmailShareButton>
      </ShareLinks>
    </Wrapper>
  );
};

export default ShareIcons;
