import React, { Component } from 'react';
import Img from 'gatsby-image';
import styled from 'styled-components';

import { GalleryModal } from '.';

const Wrapper = styled.div`
  width: 65%;
  height: calc(100vh - 63px);
  padding: 5px;
  min-width: 0px;
  min-height: 0px;
  overflow: scroll;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    display: none;
  }
`;

const Images = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 5px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    grid-template-columns: 1fr 1fr;
  }
  button {
    border: 0;
    border: none;
    padding: 0;
    outline: none;
  }
`;

const GalleryImage = styled(Img)`
  && {
    cursor: pointer;
    height: 250px;
    &:after {
      transition: all 1s ease-in-out;
      content: '';
      opacity: 0;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 2;
      background: ${props => props.theme.palette.background.overlay};
    }
    img {
      transform: scale(1);
      transition: all 1s cubic-bezier(0.59, 0, 0.06, 1) 0s !important;
    }
    &:hover {
      &:after {
        opacity: 1;
      }
      img {
        transform: scale(1.1);
      }
    }
  }
`;

class GalleryDesktop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeImage: null
    };
  }

  handleModalOpen = event => {
    const { openModal } = this.props;
    const imageindex = event.currentTarget.dataset.imageid;
    this.setState({
      activeImage: imageindex
    });
    openModal(event);
  };

  render() {
    const { activeImage } = this.state;
    const {
      companyInfo,
      locationInfo,
      location,
      closeModal,
      showModal,
      imageindex
    } = this.props;

    return (
      <Wrapper>
        <Images>
          {locationInfo.imageGallery.map((i, index) => (
            <button
              key={i.id}
              type="button"
              data-imageid={index}
              onClick={this.handleModalOpen}
            >
              <GalleryImage fluid={i.fluid} alt={i.description} />
            </button>
          ))}
        </Images>
        {showModal && imageindex === activeImage && (
          <GalleryModal
            showModal={showModal}
            closeModal={closeModal}
            activeImage={activeImage}
            companyInfo={companyInfo}
            locationInfo={locationInfo}
            location={location}
          />
        )}
      </Wrapper>
    );
  }
}

export default GalleryDesktop;
