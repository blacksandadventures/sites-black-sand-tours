import Header from './Header';
import LocationShare from './LocationShare';
import Container from './Container';
import GalleryDesktop from './GalleryDesktop';
import GalleryModal from './GalleryModal';
import ModalSlider from './ModalSlider';
import SliderMobile from './SliderMobile';
import Details from './Details';
import DetailsShare from './DetailsShare';

export {
  Header,
  LocationShare,
  Container,
  GalleryDesktop,
  GalleryModal,
  ModalSlider,
  SliderMobile,
  Details,
  DetailsShare
};
