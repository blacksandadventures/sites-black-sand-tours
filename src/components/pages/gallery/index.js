import React, { Component, Fragment } from 'react';
import { StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

import { Galleries } from './sections';

const ExternalGalleryLink = styled.div`
  display: flex;
  justify-content: center;
  a {
    color: ${props => props.theme.palette.text.secondary};
    font-family: ${props => props.theme.font.header};
    font-size: 0.75em;
    line-height: 1.2em;
    text-transform: uppercase;
    display: inline-block;
    position: relative;
    letter-spacing: 0.25em;
    background: ${props => props.theme.palette.primary.main};
    padding: 2em 2.5em;
    margin: 30px 0 0;
    &:hover {
      color: ${props => props.theme.palette.text.secondary};
      background: ${props => props.theme.palette.primary.hover};
    }
  }
`;

class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      galleryid: '',
      imageindex: null,
      showModal: false
    };
  }

  handleModalOpen = event => {
    const galleryid = event.currentTarget.dataset.galleryid;
    const imageindex = event.currentTarget.dataset.imageid;
    this.setState({
      showModal: true,
      imageindex,
      galleryid
    });
  };

  handleModalClose = () => {
    this.setState({
      showModal: false,
      imageindex: null,
      galleryid: ''
    });
  };

  render() {
    const { galleryid, imageindex, showModal } = this.state;
    return (
      <StaticQuery
        query={graphql`
          query {
            allContentfulImageGallery(sort: { fields: [title], order: ASC }) {
              edges {
                node {
                  id
                  title
                  images {
                    id
                    title
                    description
                    fluid(maxWidth: 1500) {
                      ...GatsbyContentfulFluid_withWebp_noBase64
                    }
                  }
                }
              }
            }
            contentfulCompanyInformation(
              id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
            ) {
              email
              phone
              rezdyBookingLink
              google
            }
          }
        `}
        render={data => {
          const images = data.allContentfulImageGallery.edges;
          const companyInfo = data.contentfulCompanyInformation;
          return (
            <Fragment>
              <Galleries
                images={images}
                showModal={showModal}
                galleryid={galleryid}
                imageindex={imageindex}
                companyInfo={companyInfo}
                openModal={this.handleModalOpen}
                closeModal={this.handleModalClose}
              />
              <ExternalGalleryLink>
                <a
                  href={companyInfo.google}
                  target="_blank"
                  rel="noopener noreferrer"
                  title="View all our tour photos"
                >
                  View all our photos
                </a>
              </ExternalGalleryLink>
            </Fragment>
          );
        }}
      />
    );
  }
}

export default Gallery;
