import React from 'react';
import { compose, withProps, withStateHandlers } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import styled from 'styled-components';

const { InfoBox } = require('react-google-maps/lib/components/addons/InfoBox');

const Wrapper = styled.div`
  width: 100%;
  position: relative;
  > div.infoBox {
    overflow: visible;
  }
`;

const MarkerText = styled.p`
  font-family: 'Nunito', sans-serif;
  font-weight: 700;
  font-size: 0.75em;
  line-height: 1.2em;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  letter-spacing: 0.25em;
  a {
    color: #ffffff;
    &:hover {
      opacity: 0.8;
    }
  }
`;

const GooglePickupMap = compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyCV1uaze1TnmkJyVt1rMJF4xQmlCCVLcIE&v=3.exp&libraries=geometry,drawing,places',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: (
      <div
        style={{
          height: `500px`,
          borderRadius: `6px`,
          overflow: `hidden`,
          border: `4px solid #f1f1f1`
        }}
      />
    ),
    mapElement: <div style={{ height: `100%` }} />,
    center: { lat: -36.846694, lng: 174.76419 }
  }),
  withStateHandlers(
    () => ({
      isOpen: true
    }),
    {
      onToggleOpen: ({ isOpen }) => () => ({
        isOpen: !isOpen
      })
    }
  ),
  withScriptjs,
  withGoogleMap
)(props => {
  const { pickupLocations } = props;
  return (
    <Wrapper>
      <GoogleMap
        options={{ gestureHandling: 'cooperative' }}
        defaultZoom={15}
        defaultCenter={props.center}
      >
        {pickupLocations.map(({ node: pickupLocation }) => {
          const { id, name, location, mapLink } = pickupLocation;
          return (
            <InfoBox
              key={id}
              position={new google.maps.LatLng(location.lat, location.lon)}
              onClick={props.onToggleOpen}
              options={{ closeBoxURL: ``, enableEventPropagation: true }}
            >
              <div
                style={{
                  borderRadius: `0 6px 6px`,
                  backgroundColor: `rgba(69, 152, 156, 0.8)`,
                  padding: `1px`,
                  textAlign: `center`
                }}
              >
                <div
                  style={{
                    fontSize: `14px`,
                    color: `#ffffff`,
                    width: `190px`
                  }}
                >
                  <MarkerText>
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={mapLink}
                      title={name}
                    >
                      {name}
                    </a>
                  </MarkerText>
                </div>
              </div>
            </InfoBox>
          );
        })}
      </GoogleMap>
    </Wrapper>
  );
});

export default GooglePickupMap;
