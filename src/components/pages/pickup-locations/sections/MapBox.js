import React from 'react';
import MapGL, { Marker } from 'react-map-gl';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 100%;
  position: relative;
`;

const PickupMaker = styled.div`
  position: relative;
  height: 10px;
  width: 10px;
  padding: 1em;
  border-radius: 50%;
  transform: translate(0, 0);
  background: ${props => props.theme.palette.primary.overlay};
  text-decoration: none;
  text-align: left;
  z-index: 1;
`;

const Text = styled.h6`
  width: 250px;
  position: absolute;
  top: 11px;
  left: 50px;
`;

class MapBox extends React.Component {
  state = {
    mapStyle: 'mapbox://styles/discovrbookings/cjq5eb2g6akc32rqeuf3yecd2',
    viewport: {
      zoom: 14,
      bearing: 0,
      pitch: 0
    },
    settings: {
      scrollZoom: false,
      touchZoom: false,
      touchRotate: false,
      keyboard: true,
      doubleClickZoom: true,
      minZoom: 0,
      maxZoom: 20,
      minPitch: 0,
      maxPitch: 85
    }
  };

  _onViewportChange = viewport => this.setState({ viewport });

  render() {
    const { viewport, settings, mapStyle } = this.state;
    const { pickupLocations, companyInformation } = this.props;
    const { lat, lon } = companyInformation.coordinates;
    return (
      <Wrapper>
        <MapGL
          {...viewport}
          {...settings}
          latitude={lat}
          longitude={lon}
          width="100%"
          height="82vh"
          mapStyle={mapStyle}
          onViewportChange={this._onViewportChange}
          mapboxApiAccessToken="pk.eyJ1IjoiZGlzY292cmJvb2tpbmdzIiwiYSI6ImNqcTVheWcxZjIycnE0OW56d3Uzc2xsNjEifQ.t6smyKqwsErsHfi34PeXTQ"
        >
          {pickupLocations.map(({ node: pickupLocation }) => {
            const { id, name, location } = pickupLocation;
            return (
              <Marker
                key={id}
                latitude={location.lat}
                longitude={location.lon}
                offsetLeft={-20}
                offsetTop={-10}
              >
                <PickupMaker>
                  <Text>{name}</Text>
                </PickupMaker>
              </Marker>
            );
          })}
        </MapGL>
      </Wrapper>
    );
  }
}

export default MapBox;
