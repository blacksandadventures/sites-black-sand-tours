import React, { Component } from 'react';
import styled from 'styled-components';
import { StaticQuery, graphql } from 'gatsby';

import ToggleImages from '../../../assets/images/ui/plus_minus.png';

const Wrapper = styled.div`
  border-top: 1px solid ${props => props.theme.palette.border.dark};
  margin-top: 40px;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    margin-top: 0;
  }
`;

const Toggle = styled.div`
  cursor: pointer;
  border-bottom: 1px solid ${props => props.theme.palette.border.dark};
`;

const Question = styled.h3`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row-reverse;
  margin: 0;
  i {
    width: 24px;
    min-width: 24px;
    height: 24px;
    background: url(${ToggleImages}) 0px -24px no-repeat;
    transition: all 0.3s ease 0s;
    margin: 20px;
  }
  span {
    display: block;
    color: ${props => props.theme.palette.text.black};
    padding: 25px;
    font-size: 20px;
    line-height: 1.25;
    cursor: pointer;
    font-weight: 200;
    margin: 0px;
  }
  &.active {
    i {
      background: url(${ToggleImages}) 0 0 no-repeat;
    }
  }
`;

const Answer = styled.div`
  display: none;
  padding: 7px 25px 10px;
  margin: -7px 0px 6px;
  ul {
    li {
      margin-bottom: 15px;
    }
  }
  &.active {
    display: block;
  }
`;

class FAQ extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedid: null
    };
  }

  handleOpen = id => {
    const { selectedid } = this.state;
    const isSame = selectedid === id;
    if (!isSame) {
      this.setState({
        selectedid: id
      });
    } else {
      this.setState({
        selectedid: null
      });
    }
  };

  render() {
    const { selectedid } = this.state;
    return (
      <StaticQuery
        query={graphql`
          query {
            allContentfulFrequentlyAskedQuestions {
              edges {
                node {
                  id
                  question
                  answer {
                    childMarkdownRemark {
                      html
                    }
                  }
                }
              }
            }
          }
        `}
        render={data => {
          const questions = data.allContentfulFrequentlyAskedQuestions.edges;
          return (
            <Wrapper>
              {questions.map(({ node: question }) => (
                <Toggle
                  key={question.id}
                  onClick={() => this.handleOpen(question.id)}
                >
                  <Question
                    className={question.id === selectedid ? 'active' : ''}
                  >
                    <i />
                    <span>{question.question}</span>
                  </Question>
                  <Answer
                    className={question.id === selectedid ? 'active' : ''}
                  >
                    <div
                      dangerouslySetInnerHTML={{
                        __html: question.answer.childMarkdownRemark.html
                      }}
                    />
                  </Answer>
                </Toggle>
              ))}
            </Wrapper>
          );
        }}
      />
    );
  }
}

export default FAQ;
