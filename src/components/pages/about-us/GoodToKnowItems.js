import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';
import {
  LocationsIcon,
  ClockIcon,
  PickupIcon
} from '../../../assets/images/siteIcons';

const Wrapper = styled.ul`
  position: relative;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 20px;
  text-align: center;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    grid-template-columns: 1fr;
  }
`;

const Item = styled.li`
  text-align: center;
  padding: 0px 8%;
  h3 {
    color: ${props => props.theme.palette.text.black};
  }
  svg {
    display: block;
    margin: 2em auto;
    max-height: 60px;
    path {
      fill: ${props => props.theme.palette.text.grey};
    }
  }
  div {
    a {
      &:after {
        content: ', ';
      }
      &:last-child {
        &:after {
          content: '.';
        }
      }
    }
    span {
      &:after {
        content: ', ';
      }
      &:last-child {
        &:after {
          content: ' ';
        }
      }
    }
    li {
      &:nth-child(3n) {
        margin-bottom: 1em;
      }
      &:last-child {
        margin-bottom: 0;
      }
    }
    p {
      margin: 0;
    }
    strong {
      color: ${props => props.theme.palette.text.black};
    }
  }
  span {
    a {
      font-family: ${props => props.theme.font.header};
      line-height: 1.2em;
      text-transform: uppercase;
      display: inline-block;
      position: relative;
      font-size: 90%;
      color: ${props => props.theme.palette.text.black};
      letter-spacing: 0.25em;
      padding-bottom: 0.6em;
      margin-top: 2em;
      &:hover {
        color: ${props => props.theme.palette.primary.main};
      }
      &:after {
        content: '';
        position: absolute;
        right: 0px;
        bottom: 0px;
        left: 0px;
        height: 1px;
        background-color: ${props => props.theme.palette.text.black};
      }
    }
  }
`;

const GoodToKnowItems = props => {
  const { contentSection } = props;
  return (
    <StaticQuery
      query={graphql`
        query {
          allContentfulTourLocations {
            edges {
              node {
                id
                seoContent {
                  pageTitle
                  slug
                }
              }
            }
          }
          allContentfulPickupLocations {
            edges {
              node {
                id
                name
              }
            }
          }
        }
      `}
      render={data => {
        const tourLocations = data.allContentfulTourLocations;
        const pickupLocations = data.allContentfulPickupLocations;
        return (
          <Wrapper>
            <Item>
              <h3>{contentSection[2].name}</h3>
              {LocationsIcon}
              <div>
                <strong>{contentSection[2].bodyType.bodyType}</strong>
                <br />
                {tourLocations.edges.map(({ node: location }) => (
                  <Link
                    key={location.id}
                    to={location.seoContent.slug}
                    title={location.seoContent.pageTitle}
                  >
                    {location.seoContent.pageTitle}
                  </Link>
                ))}
              </div>
              <span>
                <Link
                  to={`${contentSection[2].link.seoContent.slug}/`}
                  title={contentSection[2].link.seoContent.pageTitle}
                >
                  {contentSection[2].link.seoContent.pageTitle}
                </Link>
              </span>
            </Item>
            <Item>
              <h3>{contentSection[3].name}</h3>
              {ClockIcon}
              <div
                dangerouslySetInnerHTML={{
                  __html: contentSection[3].bodyType.childMarkdownRemark.html
                }}
              />
              <span>
                <Link
                  to={`${contentSection[3].link.seoContent.slug}/`}
                  title={contentSection[3].link.seoContent.pageTitle}
                >
                  {contentSection[3].link.seoContent.pageTitle}
                </Link>
              </span>
            </Item>
            <Item>
              <h3>{contentSection[4].name}</h3>
              {PickupIcon}
              <div>
                <strong>{contentSection[4].bodyType.bodyType}</strong>
                <br />
                {pickupLocations.edges.map(({ node: location }) => (
                  <span key={location.id}>{location.name}</span>
                ))}
                and other Auckland locations.
              </div>
              <span>
                <Link
                  to={`${contentSection[4].link.seoContent.slug}/`}
                  title={contentSection[4].link.seoContent.pageTitle}
                >
                  {contentSection[4].link.seoContent.pageTitle}
                </Link>
              </span>
            </Item>
          </Wrapper>
        );
      }}
    />
  );
};

export default GoodToKnowItems;
