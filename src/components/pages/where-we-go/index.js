import React from 'react';

import { WhereWrapper, WhereItems } from './sections';

const WhereWeGoItems = () => (
  <WhereWrapper>
    <WhereItems />
  </WhereWrapper>
);
export default WhereWeGoItems;
