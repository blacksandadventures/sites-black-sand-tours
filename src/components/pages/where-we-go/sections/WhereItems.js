import React, { Fragment } from 'react';
import Img from 'gatsby-image';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

const Item = styled.li`
  position: relative;
  text-align: center;
  cursor: pointer;
  overflow: hidden;
  img {
    transform: scale(1);
    transition: all 1s cubic-bezier(0.59, 0, 0.06, 1) 0s !important;
  }
  &:hover {
    figcaption {
      &:before {
        opacity: 1;
        transform: translate3d(0px, 0px, 0px);
      }
    }
    img {
      transform: scale(1.1);
    }
    h2 {
      color: rgb(255, 255, 255);
      transform: translate3d(0px, -50%, 0px) translate3d(0px, -40px, 0px);
    }
    p {
      opacity: 1;
      transform: translate3d(0px, 0px, 0px);
    }
    button {
      opacity: 0;
      visibility: hidden;
    }
  }
`;

const LocationImage = styled(Img)`
  height: 300px;
  &:after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: ${props => props.theme.palette.background.overlay};
    content: '';
    z-index: 1;
  }
`;

const FigCaption = styled.figcaption`
  color: ${props => props.theme.palette.text.secondary};
  backface-visibility: hidden;
  &:before {
    position: absolute;
    content: '';
    opacity: 0;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transform: translate3d(0px, 50%, 0px);
    transition: opacity 0.35s ease 0s, transform 0.35s ease 0s;
    background: -webkit-linear-gradient(
      top,
      rgba(85, 187, 192, 0) 0%,
      rgba(0, 0, 0, 0.8) 75%
    );
  }
`;

const MenuLink = styled(Link)`
  color: ${props => props.theme.palette.text.secondary};
  display: flex;
  justify-content: center;
  position: absolute;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
  flex-flow: column wrap;
  z-index: 99;
  h2 {
    font-weight: 300;
    color: ${props => props.theme.palette.text.secondary};
    margin: 0px;
    transition: opacity 0.35s ease 0s, transform 0.35s ease 0s;
  }
  p {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    opacity: 0;
    transform: translate3d(0px, 10px, 0px);
    transition: opacity 0.35s ease 0s, transform 0.35s ease 0s;
    padding: 2em;
  }
`;

const Button = styled.button`
  width: 40%;
  position: absolute;
  bottom: 50px;
  left: 50%;
  transform: translateX(-50%);
  line-height: 1;
  font-size: 14px;
  font-family: ${props => props.theme.font.header};
  color: ${props => props.theme.palette.text.secondary};
  background: transparent;
  border: 1px solid ${props => props.theme.palette.background.light};
  padding: 10px;
  z-index: 99;
  transition: opacity 0.35s ease 0s;
`;

const WhereItems = () => (
  <StaticQuery
    query={graphql`
      query {
        allContentfulTourLocations {
          edges {
            node {
              id
              tagLine
              seoContent {
                pageTitle
                slug
                featuredImage {
                  title
                  description
                  fluid(maxWidth: 800) {
                    ...GatsbyContentfulFluid_withWebp_noBase64
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => {
      const locations = data.allContentfulTourLocations.edges;
      return (
        <Fragment>
          {locations.map(({ node: location }) => (
            <Item key={location.id}>
              <LocationImage
                fluid={location.seoContent.featuredImage.fluid}
                title={location.seoContent.featuredImage.description}
              />
              <FigCaption>
                <MenuLink
                  to={`where-we-go/${location.seoContent.slug}/`}
                  title={location.seoContent.pageTitle}
                >
                  <h2>{location.seoContent.pageTitle}</h2>
                  <p>{location.tagLine}</p>
                </MenuLink>
              </FigCaption>
              <Button>View Location</Button>
            </Item>
          ))}
        </Fragment>
      );
    }}
  />
);

export default WhereItems;
