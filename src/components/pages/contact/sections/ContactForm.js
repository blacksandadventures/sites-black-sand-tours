import React, { Fragment } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import PhoneInput from 'react-phone-number-input';
import 'react-phone-number-input/style.css';

const FormContainer = styled.form``;

const FormFields = styled.ul`
  width: 100%;
  list-style: none;
  display: grid;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
  grid-template-columns: 1fr 1fr;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    grid-template-columns: 1fr;
  }
  li {
    .react-phone-number-input {
      position: relative;
      background: ${props => props.theme.palette.background.light};
      box-sizing: border-box;
      width: 100%;
      border: 1px solid ${props => props.theme.palette.border.dark};
      font-size: 14px;
      line-height: 20px;
      letter-spacing: 1px;
      color: ${props => props.theme.palette.text.primary};
      border-radius: 0;
      transition: all 300ms ease-in-out;
      &:hover,
      &:focus {
        border: 1px solid ${props => props.theme.palette.primary.main};
        .react-phone-number-input__row {
          .react-phone-number-input__country {
            border-color: ${props => props.theme.palette.primary.main};
          }
        }
      }
      .react-phone-number-input__row {
        height: 48px;
        .react-phone-number-input__country {
          align-self: auto;
          height: 100%;
          background: ${props => props.theme.palette.background.middle};
          width: 45px;
          border-right: 1px solid ${props => props.theme.palette.border.dark};
          transition: all 300ms ease-in-out;
          .react-phone-number-input__icon {
            border: none;
            width: 39px;
            height: 40px;
            display: flex;
            padding: 8px;
          }
          .react-phone-number-input__country-select-arrow {
            margin-right: 0.3em;
            margin-left: 0;
          }
        }
        input {
          border: none;
          padding: 0 0 0 8px;
          &:hover,
          &:focus {
            border: none;
          }
        }
      }
    }
    label {
      font-family: ${props => props.theme.font.header};
      color: ${props => props.theme.palette.text.grey};
      font-size: 14px;
    }
    input {
      outline: none;
      font-family: ${props => props.theme.font.header};
      background: ${props => props.theme.palette.background.light};
      box-sizing: border-box;
      width: 100%;
      border: 1px solid ${props => props.theme.palette.border.dark};
      font-size: 14px;
      line-height: 20px;
      letter-spacing: 1px;
      color: ${props => props.theme.palette.text.primary};
      padding: 14px;
      resize: none;
      -webkit-appearance: none;
      border-radius: 0;
      transition: all 300ms ease-in-out;
      &:hover,
      &:focus {
        border: 1px solid ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const MessageField = styled.ul`
  width: 100%;
  list-style: none;
  margin-top: 10px;
  display: grid;
  grid-column-gap: 10px;
  grid-row-gap: 5px;
  grid-template-columns: 1fr;
  li {
    label {
      font-family: ${props => props.theme.font.header};
      color: ${props => props.theme.palette.text.grey};
      font-size: 14px;
    }
    &.show {
      background: ${props => props.theme.palette.background.light};
      border: 1px solid ${props => props.theme.palette.border.dark};
      margin: 0 0 15px;
      width: auto;
      &:hover {
        border: 1px solid ${props => props.theme.palette.primary.main};
      }
      input[type='checkbox'] {
        display: none;
      }
      label {
        opacity: 1;
        visibility: visible;
        height: 50px;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        cursor: pointer;
        position: relative;
        z-index: 9;
        &:hover {
          span {
            border-left: 1px solid ${props => props.theme.palette.border.dark};
            &:before {
              width: 12px;
              transition: width 100ms ease;
            }
            &:after {
              width: 25px;
              transition: width 150ms ease 100ms;
            }
          }
        }
        p {
          padding: 20px 1em 15px;
          margin: 0;
        }
        span {
          position: relative;
          background-color: transparent;
          width: 50px;
          height: 50px;
          transform-origin: center;
          border-left: 1px solid ${props => props.theme.palette.border.dark};
          vertical-align: -6px;
          transition: background-color 150ms 200ms,
            transform 350ms cubic-bezier(0.78, -1.22, 0.17, 1.89);
          &:before {
            content: '';
            width: 0px;
            height: 2px;
            border-radius: 2px;
            background: ${props => props.theme.palette.primary.main};
            position: absolute;
            transform: rotate(45deg);
            top: 26px;
            left: 15px;
            transition: width 50ms ease 50ms;
            transform-origin: 0% 0%;
          }
          &:after {
            content: '';
            width: 0;
            height: 2px;
            border-radius: 2px;
            background: ${props => props.theme.palette.primary.main};
            position: absolute;
            transform: rotate(305deg);
            top: 35px;
            left: 22px;
            transition: width 50ms ease;
            transform-origin: 0% 0%;
          }
        }
      }
      &.checked {
        label {
          span {
            background-color: ${props => props.theme.palette.primary.main};
            transition: all 300ms ease-in-out;
            &:after {
              width: 25px;
              background: ${props => props.theme.palette.background.light};
              transition: all 300ms ease-in-out;
            }
            &:before {
              width: 12px;
              background: ${props => props.theme.palette.background.light};
              transition: all 300ms ease-in-out;
            }
          }
          &:hover {
            span {
              background-color: ${props => props.theme.palette.primary.main};
              &:after {
                width: 25px;
                background: ${props => props.theme.palette.background.light};
              }
              &:before {
                width: 12px;
                background: ${props => props.theme.palette.background.light};
              }
            }
          }
        }
      }
    }
    textarea {
      font-family: ${props => props.theme.font.header};
      outline: none;
      box-sizing: border-box;
      width: 100%;
      border: 1px solid ${props => props.theme.palette.border.dark};
      font-size: 14px;
      line-height: 20px;
      letter-spacing: 1px;
      color: ${props => props.theme.palette.text.primary};
      padding: 14px;
      resize: none;
      -webkit-appearance: none;
      transition: all 300ms ease-in-out;
      &:hover,
      &:focus {
        border: 1px solid ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const Submit = styled.input`
  outline: none;
  box-shadow: none;
  background: ${props => props.theme.palette.primary.main};
  color: ${props => props.theme.palette.text.secondary};
  font-size: 13px;
  letter-spacing: 2px;
  line-height: 20px;
  text-transform: uppercase;
  padding: 14px 30px;
  cursor: pointer;
  transition: all 0.3s ease;
  -webkit-appearance: none;
  border-radius: 0;
  resize: none;
  border: none;
  &:hover {
    background: ${props => props.theme.palette.primary.hover};
  }
  &:disabled {
    cursor: not-allowed;
    &:hover {
      background: ${props => props.theme.palette.primary.main};
    }
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-left: 50%;
    transform: translateX(-50%);
  }
`;

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');
}

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newsLetter: true,
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      message: ''
    };
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleCheck = () => {
    this.setState(state => ({ newsLetter: !state.newsLetter }));
  };

  handleSubmit = e => {
    const {
      newsLetter,
      firstName,
      lastName,
      email,
      phone,
      message
    } = this.state;
    e.preventDefault();
    const form = e.target;
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({
        'form-name': form.getAttribute('name'),
        firstName,
        lastName,
        email,
        phone,
        message,
        newsLetter
      })
    })
      .then(() => {
        if (newsLetter) {
          const proxyurl = 'https://cors-anywhere.herokuapp.com/';
          const url =
            'https://us12.api.mailchimp.com/3.0/lists/c7273bf25f/members';
          axios({
            url: proxyurl + url,
            method: 'POST',
            headers: {
              'cache-control': 'no-cache',
              Authorization:
                'Basic YW55c3RyaW5nOjMyYjY3NDA2M2UxM2VhMTRhYmNiNDIwNjcxNjViMTRlLXVzMTI='
            },
            data: {
              email_address: email,
              status: 'subscribed',
              merge_fields: {
                FNAME: firstName,
                LNAME: lastName,
                PHONE: phone
              }
            }
          });
        }
        alert(`Your message has been sent and we'll reply shortly`);
      })
      .catch(error => alert(error));
  };

  render() {
    const {
      newsLetter,
      firstName,
      lastName,
      email,
      phone,
      message
    } = this.state;

    const formVerified =
      firstName === '' ||
      lastName === '' ||
      email === '' ||
      phone === '' ||
      message === '';

    return (
      <Fragment>
        <FormContainer
          name="Contact-Form"
          method="post"
          action="#"
          data-netlify="true"
          data-netlify-honeypot="bot-field"
          onSubmit={this.handleSubmit}
        >
          {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
          <input type="hidden" name="form-name" value="contact" />
          <p hidden>
            <label>
              Don’t fill this out:{''}
              <input name="bot-field" onChange={this.handleChange} />
            </label>
          </p>
          <FormFields>
            <li>
              <label htmlFor="firstName">First Name</label>
              <input
                required
                name="firstName"
                id="firstName"
                type="text"
                placeholder="First Name*"
                onChange={this.handleChange}
              />
            </li>
            <li>
              <label htmlFor="lastName">Last Name</label>
              <input
                required
                name="lastName"
                id="lastName"
                type="text"
                placeholder="Last Name*"
                onChange={this.handleChange}
              />
            </li>
            <li>
              <label htmlFor="email">Email Address</label>
              <input
                required
                name="email"
                id="email"
                type="email"
                placeholder="Email Address*"
                onChange={this.handleChange}
              />
            </li>
            <li>
              <label htmlFor="phone">Phone Number</label>
              <PhoneInput
                name="phone"
                id="phone"
                type="text"
                placeholder="Phone Number"
                country="NZ"
                onChange={value => this.setState({ phone: value })}
              />
            </li>
          </FormFields>
          <MessageField>
            <li>
              <label htmlFor="message">Message</label>
              <textarea
                required
                name="message"
                id="message"
                placeholder="Message:*"
                rows="5"
                cols="50"
                onChange={this.handleChange}
              />
            </li>
            <li className={`show ${!newsLetter ? '' : 'checked'}`}>
              <label htmlFor="newsletter">
                <p>Subscribe to our newsletter?</p>
                <span />
              </label>
              <input
                name="newsletter"
                id="newsletter"
                type="checkbox"
                checked={newsLetter}
                onChange={this.handleCheck}
              />
            </li>
          </MessageField>
          <Submit
            disabled={formVerified}
            type="submit"
            id="submit"
            value={!formVerified ? 'Send Message' : 'Fill Form to Send'}
          />
        </FormContainer>
      </Fragment>
    );
  }
}

export default ContactForm;
