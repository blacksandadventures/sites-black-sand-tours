import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.ul`
  position: relative;
  list-style-type: none;
  padding-left: 0px;
  display: grid;
  grid-gap: 15px;
  grid-template-columns: 1fr 1fr 1fr;
  @media screen and (max-width: 47.9375em) {
    grid-template-columns: 1fr 1fr;
  }
  li {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    text-align: center;
    background: rgb(252, 252, 252);
    border-radius: 6px;
    padding: 20px;
    img {
      filter: brightness(0%);
      width: 50px;
      overflow: hidden;
      margin-bottom: 20px;
    }
    h4 {
      font-size: 100%;
    }
  }
`;

const ActivitesTab = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      {contentSection.map(i => (
        <li key={i.title}>
          <img src={i.icon} alt={i.title} />
          <h4>{i.title}</h4>
        </li>
      ))}
    </Wrapper>
  );
};

export default ActivitesTab;
