import TourTabs from './TourTabs';
import TourDetails from './TourDetails';

import OverviewTab from './OverviewTab';
import ActivitiesTab from './ActivitiesTab';
import IntineraryTab from './IntineraryTab';
import BringTab from './BringTab';

export {
  TourTabs,
  TourDetails,
  OverviewTab,
  ActivitiesTab,
  IntineraryTab,
  BringTab
};
