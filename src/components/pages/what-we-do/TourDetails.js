import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  h2 {
    color: ${props => props.theme.palette.text.primaryBlend};
    text-align: center;
  }
  ul {
    list-style-type: none;
    margin: 0px;
    padding: 0px;
    text-align: center;
    li {
      display: inline-block;
      text-transform: uppercase;
      vertical-align: top;
      padding: 0px 2%;
      @media screen and (max-width: 73.6875em) {
        margin-bottom: 0.75em;
      }
      &:not(:last-child) {
        border-right: 1px solid rgba(0, 0, 0, 0.23);
      }
      @media screen and (max-width: 28.7em) {
        &:nth-child(3) {
          border-right: none;
        }
      }
      h2 {
        color: ${props => props.theme.palette.text.secondary};
        margin-bottom: 0.3em;
      }
      span {
        display: block;
        line-height: 1;
        color: ${props => props.theme.palette.text.primaryBlend};
      }
    }
  }
`;

const TourDetails = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      <h2>{contentSection.name}</h2>
      <ul>
        {contentSection.jsonData.type.map(i => (
          <li key={i.fact}>
            <h2>{i.fact}</h2>
            <span>{i.name}</span>
          </li>
        ))}
      </ul>
    </Wrapper>
  );
};

export default TourDetails;
