import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { OverviewTab, ActivitiesTab, BringTab } from '.';
import IntineraryTab from './IntineraryTab';

const Wrapper = styled(Tabs)`
  figure {
    display: flex;
    margin: 20px 0px 2rem;
    .gatsby-image-wrapper {
      -webkit-box-flex: 1;
      object-fit: cover;
      flex: 1 1 0%;
      object-position: center center;
      @media screen and (max-width: 47.9375em) {
        display: none;
      }
    }
    > span {
      -webkit-box-flex: 2;
      text-align: left;
      margin: 2rem;
      flex: 2 1 0%;
      margin: 2rem 0px 2rem 2rem;
      @media screen and (max-width: 47.9375em) {
        -webkit-box-flex: 1 !important;
        margin: 0px !important;
        flex: 1 1 0% !important;
        text-align: center;
      }
      h3,
      h4 {
        color: ${props => props.theme.palette.text.black};
      }
      > p {
        margin-bottom: 20px;
      }
      h4 {
        margin-bottom: 20px;
      }
    }
  }
`;

const TabSelect = styled(TabList)`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 100px;
  @media screen and (max-width: 47.9375em) {
    flex-direction: column;
    margin-bottom: 40px;
  }
  li {
    padding: 15px 20px;
    font-family: ${props => props.theme.font.header};
    background: ${props => props.theme.palette.border.light};
    color: ${props => props.theme.palette.text.grey};
    font-size: 0.875em;
    line-height: 1.2em;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    border: 0;
    border-top: 1px solid ${props => props.theme.palette.border.dark};
    border-right: 1px solid ${props => props.theme.palette.border.dark};
    border-bottom: 1px solid ${props => props.theme.palette.border.dark};
    @media screen and (max-width: 47.9375em) {
      width: 90%;
    }
    @media screen and (max-width: 47.9375em) {
      border-left: 1px solid ${props => props.theme.palette.border.dark};
    }
    &.react-tabs__tab--selected {
      border-color: ${props => props.theme.palette.border.dark};
      color: ${props => props.theme.palette.primary.main};
      border-radius: 0;
    }
    &:first-child {
      padding: 15px 20px 15px 25px;
      border-left: 1px solid ${props => props.theme.palette.border.dark};
      border-radius: 20px 0 0 20px;
      @media screen and (max-width: 47.9375em) {
        border-radius: 0;
        padding: 15px 20px;
      }
    }
    &:last-child {
      padding: 15px 25px 15px 20px;
      border-radius: 0 20px 20px 0;
      @media screen and (max-width: 47.9375em) {
        border-radius: 0;
        padding: 15px 20px;
      }
    }
  }
`;

const TourTabs = props => {
  const { tabContentSection } = props;
  return (
    <Wrapper>
      <TabSelect>
        {tabContentSection.map(i => (
          <Tab key={i.name}>{i.name}</Tab>
        ))}
      </TabSelect>
      {tabContentSection.map(i => (
        <TabPanel key={i.name}>
          <figure>
            <Img
              fluid={i.image.fluid}
              title={i.image.title}
              alt={i.image.description}
            />
            <span>
              <h3>{i.jsonData.title}</h3>
              {i.jsonData.text !== null && <p>{i.jsonData.text}</p>}
              {i.jsonData.linksHeader !== null && (
                <h4>{i.jsonData.linksHeader}</h4>
              )}
              {i.name === 'Overview' && (
                <OverviewTab contentSection={i.jsonData.links} />
              )}
              {i.name === 'Activities' && (
                <ActivitiesTab contentSection={i.jsonData.links} />
              )}
              {i.name === 'Itinerary' && (
                <IntineraryTab contentSection={i.jsonData.links} />
              )}
              {i.name === 'What to bring' && (
                <BringTab contentSection={i.jsonData.links} />
              )}
            </span>
          </figure>
        </TabPanel>
      ))}
    </Wrapper>
  );
};

export default TourTabs;
