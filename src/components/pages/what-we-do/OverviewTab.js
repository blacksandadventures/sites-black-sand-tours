import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

const Wrapper = styled.ul`
  li {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background: ${props => props.theme.palette.border.light};
    border-radius: 6px;
    margin-bottom: 1em;
    overflow: hidden;
    @media screen and (max-width: 47.9375em) {
      flex-direction: column;
    }
    div {
      padding: 15px 15px 15px 30px;
      height: 40px;
      width: 30px;
      color: rgb(160, 163, 171);
      display: inherit;
      -webkit-box-flex: 1;
      flex: 1 1 0%;
      @media screen and (max-width: 47.9375em) {
        display: none;
      }
      img {
        height: 40px;
        width: 40px;
        filter: brightness(80%);
      }
    }
    a {
      height: 100%;
      width: 100px;
      min-height: 110px;
      min-width: 100px;
      align-items: center;
      justify-content: center;
      display: inline-flex;
      font-weight: 900;
      font-family: nunito, sans-serif;
      margin: 0px;
      background: ${props => props.theme.palette.primary.main};
      color: ${props => props.theme.palette.text.secondary};
      border-radius: 0px 6px 6px 0px;
      transition: all 300ms ease-in-out;
      cursor: pointer;
      @media screen and (max-width: 47.9375em) {
        width: 100%;
        min-height: 30px;
        border-radius: 0;
      }
      &:hover {
        background: ${props => props.theme.palette.primary.hover};
        color: ${props => props.theme.palette.text.secondary};
      }
    }
    p {
      padding: 15px;
      margin: 0px;
    }
  }
`;

const OverviewTab = props => {
  const { contentSection } = props;
  return (
    <Wrapper>
      {contentSection.map(i => (
        <li key={i.text}>
          <div>
            <img src={i.icon} alt={i.text} />
          </div>
          <p>{i.text}</p>
          <Link to={i.link}>{i.linkText}</Link>
        </li>
      ))}
    </Wrapper>
  );
};

export default OverviewTab;
