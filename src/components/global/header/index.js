import React from 'react';

import {
  HeaderWrapper,
  HeaderLeft,
  MenuTrigger,
  BookingTrigger,
  ShareTrigger,
  HeadIcons,
  HeaderLogo
} from './sections';

const Header = props => {
  const { menuClick, scrolling, sharing, shareClick, seoContent } = props;
  return (
    <HeaderWrapper className={scrolling ? 'active' : ''}>
      <HeaderLeft>
        <MenuTrigger scrolling={scrolling} menuClick={menuClick} />
        <BookingTrigger scrolling={scrolling} />
      </HeaderLeft>
      <ShareTrigger
        scrolling={scrolling}
        seoContent={seoContent}
        shareClick={shareClick}
        sharing={sharing}
      />
      <HeadIcons scrolling={scrolling} />
      <HeaderLogo scrolling={scrolling} />
    </HeaderWrapper>
  );
};

export default Header;
