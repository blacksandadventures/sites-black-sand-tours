import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

import Logo from '../../../../assets/images/logos/black-sand-logo-dark.svg';
import * as ROUTES from '../../../../constants/routes';
import config from '../../../../utils/siteConfig';

const Wrapper = styled(Link)`
  display: none;
  position: absolute;
  top: 0px;
  left: 50%;
  text-indent: 101%;
  white-space: nowrap;
  z-index: 10;
  margin-left: -50px;
  background-size: contain;
  width: 85px;
  height: 72px;
  background-image: url(${Logo});
  overflow: hidden;
  background-position: center center;
  background-repeat: no-repeat;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    height: 55px;
  }
  &.active {
    display: block;
  }
`;

const HeaderLogo = props => {
  const { scrolling } = props;
  return (
    <Wrapper
      className={scrolling ? 'active' : ''}
      title={config.siteTitle}
      to={ROUTES.INDEX}
    >
      {config.siteTitle}
    </Wrapper>
  );
};

export default HeaderLogo;
