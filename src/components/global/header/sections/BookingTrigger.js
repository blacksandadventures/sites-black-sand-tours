import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

import { BookingIcon } from '../../../../assets/images/siteIcons';

const Wrapper = styled.li`
  float: left;
  &.active {
    border-bottom: 1px solid ${props => props.theme.palette.primary.main};
  }
`;

const Trigger = styled.a`
  display: block;
  padding: 0px 42px;
  background-color: ${props => props.theme.palette.primary.main};
  cursor: pointer;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    padding: 0px 30px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    height: 55px;
    width: 55px;
    padding: 0px;
  }
  svg {
    height: 20px;
    width: 20px;
    margin: 0 17px -5px 0;
    transition: all 300ms ease-in-out;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      margin: 17px;
    }
  }
  &:hover {
    svg {
      opacity: 0.8;
    }
    span {
      opacity: 0.8;
    }
  }
`;

const Text = styled.span`
  font-family: ${props => props.theme.font.header};
  color: ${props => props.theme.palette.text.secondary};
  font-weight: 700;
  font-size: 0.8em;
  line-height: 70px;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  letter-spacing: 0.25em;
  transition: all 300ms ease-in-out;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    line-height: 55px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: none;
  }
`;

const BookingTrigger = props => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          rezdyBookingLink
        }
      }
    `}
    render={data => {
      const { rezdyBookingLink } = data.contentfulCompanyInformation;
      const { scrolling } = props;
      return (
        <Wrapper className={scrolling ? 'active' : ''}>
          <Trigger
            href={rezdyBookingLink}
            target="_blank"
            rel="noopener noreferrer"
            title="Book your Black Sand Adventures Tour"
          >
            <BookingIcon />
            <Text>Book the tour</Text>
          </Trigger>
        </Wrapper>
      );
    }}
  />
);

export default BookingTrigger;
