import styled from 'styled-components';

const HeaderWrapper = styled.header`
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  z-index: 10;
  transition: background-color 0.5s ease-in-out;
  backface-visibility: visible;
  &.active {
    background-color: ${props => props.theme.palette.background.light};
    &:after {
      display: block;
    }
  }
  &:after {
    content: '';
    position: absolute;
    right: 0px;
    bottom: 0px;
    left: 0px;
    height: 1px;
    background-color: ${props => props.theme.palette.border.dark};
    display: none;
  }
`;

export default HeaderWrapper;
