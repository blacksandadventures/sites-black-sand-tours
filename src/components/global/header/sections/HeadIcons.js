import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

import {
  PhoneIcon,
  MailIcon,
  MapIcon
} from '../../../../assets/images/siteIcons';
import * as ROUTES from '../../../../constants/routes';

const Wrapper = styled.ul`
  float: right;
  height: 70px;
  position: relative;
  display: flex;
  align-items: center;
  z-index: 20;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    height: 55px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: none;
  }
  li {
    margin-right: 20px;
    &:last-child {
      margin-right: 0;
    }
  }
  &.active {
    svg {
      path {
        fill: ${props => props.theme.palette.text.primary};
      }
    }
  }
`;

const Call = styled.li`
  float: left;
  svg {
    margin: 4px 0 0;
    height: 20px;
    width: 25px;
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      height: 16px;
      width: 22px;
    }
    path {
      transition: fill 300ms ease-in-out;
      fill: ${props => props.theme.palette.text.secondary};
    }
    &:hover {
      path {
        fill: ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const Email = styled.li`
  float: left;
  svg {
    margin: 4px 0 0;
    height: 20px;
    width: 25px;
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      height: 16px;
      width: 22px;
    }
    path {
      transition: fill 300ms ease-in-out;
      fill: ${props => props.theme.palette.text.secondary};
    }
    &:hover {
      path {
        fill: ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const Map = styled.li`
  float: left;
  svg {
    margin: 4px 0 0;
    height: 20px;
    width: 25px;
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      height: 16px;
      width: 22px;
    }
    path {
      transition: fill 300ms ease-in-out;
      fill: ${props => props.theme.palette.text.secondary};
    }
    &:hover {
      path {
        fill: ${props => props.theme.palette.primary.main};
      }
    }
  }
`;

const MenuTrigger = props => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          phone
        }
      }
    `}
    render={data => {
      const { phone } = data.contentfulCompanyInformation;
      const { scrolling } = props;
      return (
        <Wrapper className={scrolling ? 'active' : ''}>
          <Call>
            <a href={`tel:${phone}`} title={phone}>
              <PhoneIcon />
            </a>
          </Call>
          <Email>
            <Link to={ROUTES.CONTACT} title="Email and contact">
              <MailIcon />
            </Link>
          </Email>
          <Map>
            <Link to={ROUTES.PICKUP_LOCATIONS} title="Tour pickup locations">
              <MapIcon />
            </Link>
          </Map>
        </Wrapper>
      );
    }}
  />
);

export default MenuTrigger;
