import React from 'react';
import styled from 'styled-components';

import { MenuIcon } from '../../../../assets/images/siteIcons';

const Wrapper = styled.li`
  float: left;
  &.active {
    border-bottom: 1px solid ${props => props.theme.palette.border.dark};
  }
`;

const Trigger = styled.div`
  display: block;
  padding: 0px 42px;
  background-color: ${props => props.theme.palette.background.light};
  cursor: pointer;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    padding: 0px 30px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    height: 55px;
    width: 55px;
    padding: 0px;
  }
  svg {
    height: 20px;
    width: 20px;
    margin: 0 17px -5px 0;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      margin: 17px;
    }
    path {
      transition: fill 300ms ease-in-out;
    }
  }
  &:hover {
    svg {
      path {
        fill: ${props => props.theme.palette.primary.main};
      }
    }
    span {
      color: ${props => props.theme.palette.primary.main};
    }
  }
`;

const Text = styled.span`
  font-family: ${props => props.theme.font.header};
  font-weight: 700;
  font-size: 0.8em;
  line-height: 70px;
  text-transform: uppercase;
  display: inline-block;
  position: relative;
  letter-spacing: 0.25em;
  transition: all 300ms ease-in-out;
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    line-height: 55px;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: none;
  }
`;

const MenuTrigger = props => {
  const { menuClick, scrolling } = props;
  return (
    <Wrapper className={scrolling ? 'active' : ''}>
      <Trigger onClick={menuClick}>
        <MenuIcon />
        <Text>Menu</Text>
      </Trigger>
    </Wrapper>
  );
};

export default MenuTrigger;
