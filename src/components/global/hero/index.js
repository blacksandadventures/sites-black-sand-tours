import React from 'react';

import {
  HeroWrapper,
  HeroImage,
  HeroLogo,
  HeroLink,
  HeroText,
  HeroVideo
} from './sections';

const Hero = props => {
  const { seoContent, contentSection, location, thankYou } = props;
  const { featuredImage, pageTitle } = seoContent;
  return (
    <HeroWrapper className={thankYou ? 'thankyou' : ''}>
      {location.pathname === '/' && contentSection && (
        <HeroVideo contentSection={contentSection} />
      )}
      {!thankYou && <HeroImage featuredImage={featuredImage} />}
      <HeroLogo />
      {contentSection && <HeroText contentSection={contentSection} />}
      {!thankYou && <HeroLink pageTitle={pageTitle} />}
    </HeroWrapper>
  );
};

export default Hero;
