import styled from 'styled-components';

const InnerContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  height: 100vh;
  width: 100%;
  overflow: auto;
  background: ${props => props.theme.palette.background.light};
  &.active {
    overflow: hidden;
  }
`;

export default InnerContainer;
