import React from 'react';
import { Link } from 'gatsby';
import Img from 'gatsby-image';
import styled from 'styled-components';
import Swiper from 'react-id-swiper';

import { ModalLeft, ModalRight } from '../../../assets/images/siteIcons';

const Wrapper = styled.div`
  z-index: 1;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-bottom: 0 !important;
  }
`;

const Inner = styled.div`
  max-width: 1080px;
  margin-left: auto;
  margin-right: auto;
  box-sizing: content-box;
  padding-right: 42px;
  padding-left: 42px;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-right: 21px;
    padding-left: 21px;
  }
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    display: flex;
    flex-direction: row-reverse;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: flex;
    flex-direction: column-reverse;
  }
  &.reversed {
    flex-direction: row;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      flex-direction: column-reverse;
    }
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

const One = styled.div`
  float: left;
  display: block;
  margin-right: 5.62516%;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    float: left;
    display: block;
    padding-top: 60px;
    margin-right: 5.62516%;
    margin-left: 5.62516%;
    width: 47.1874%;
  }
  &.reversed {
    @media screen and (min-width: ${props => props.theme.responsive.small}) {
      float: left;
      display: block;
      margin-right: 5.62516%;
      margin-left: 0;
      width: 38.3853%;
      padding-top: 60px;
    }
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-bottom: 0;
    text-align: center;
  }
  a {
    font-family: ${props => props.theme.font.header};
    line-height: 1.2em;
    text-transform: uppercase;
    display: inline-block;
    position: relative;
    font-size: 90%;
    color: ${props => props.theme.palette.text.black};
    letter-spacing: 0.25em;
    padding-bottom: 0.6em;
    &:hover {
      color: ${props => props.theme.palette.primary.main};
    }
    &:after {
      content: '';
      position: absolute;
      right: 0px;
      bottom: 0px;
      left: 0px;
      height: 1px;
      background-color: ${props => props.theme.palette.text.black};
    }
  }
`;

const Two = styled.div`
  position: relative;
  float: left;
  display: block;
  margin-right: 0px;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    margin-right: 0px;
    float: left;
    display: block;
    width: 47.1874%;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-bottom: 21px;
  }
  .gatsby-image-wrapper {
    max-height: 325px;
    height: 325px;
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      max-height: 300px;
    }
    img {
      width: 100%;
      display: block;
    }
  }
  &.reversed {
    .gatsby-image-wrapper {
      max-height: 100%;
      height: auto;
      @media screen and (max-width: ${props =>
          props.theme.responsive.medium}) and (min-width: ${props =>
          props.theme.responsive.small}) {
        max-height: 100%;
      }
      img {
        width: 100%;
        display: block;
      }
    }
    @media screen and (min-width: ${props => props.theme.responsive.small}) {
      float: left;
      display: block;
      width: 55.9895%;
      margin-right: 0px;
    }
  }
  .swiper-container {
    .swiper-pagination {
      .swiper-pagination-bullet-active {
        background: ${props => props.theme.palette.text.secondary};
      }
    }
  }
`;

const NavButton = styled.div`
  top: 50%;
  transform: translateY(-50%);
  display: none;
  padding: 0;
  width: 44px !important;
  height: 44px !important;
  background-image: none !important;
  text-align: center;
  background: rgba(0, 0, 0, 0.2);
  color: #${props => props.theme.palette.text.secondary};
  border: 2px solid ${props => props.theme.palette.background.light};
  border-radius: 50%;
  margin: 0 8px;
  transition: all 300ms ease-in-out;
  cursor: pointer;
  outline: none;
  position: absolute;
  z-index: 4;
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    display: block;
  }
  @media screen and (min-width: ${props => props.theme.responsive.large}) {
    margin: 0 40px;
  }
  &:hover {
    background: rgba(0, 0, 0, 0.5);
  }
  &.swiper-button-next {
    right: 0;
  }
  &.swiper-button-previous {
    left: 0;
  }
  span {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    svg {
      fill: none;
      stroke-width: 2;
      stroke: ${props => props.theme.palette.text.secondary};
      width: 24px;
      height: 24px;
    }
  }
`;

const BoxTop = props => {
  const { contentSection, reversed, isSlider } = props;

  const params = {
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    renderPrevButton: () => (
      <NavButton className="swiper-button-prev">
        <span>{ModalLeft}</span>
      </NavButton>
    ),
    renderNextButton: () => (
      <NavButton className="swiper-button-next">
        <span>{ModalRight}</span>
      </NavButton>
    )
  };
  return (
    <Wrapper>
      <Inner className={reversed ? 'reversed' : ''}>
        <One className={reversed ? 'reversed' : ''}>
          <h5>#blacksandadventures</h5>
          <h2>{contentSection.name}</h2>
          <div
            dangerouslySetInnerHTML={{
              __html: contentSection.bodyType.childMarkdownRemark.html
            }}
          />
          {contentSection.link !== null && (
            <Link
              title={contentSection.link.seoContent.pageTitle}
              to={`${contentSection.link.seoContent.slug}/`}
            >
              {contentSection.link.seoContent.pageTitle}
            </Link>
          )}
        </One>
        <Two className={reversed ? 'reversed' : ''}>
          {!isSlider ? (
            <Img
              fluid={contentSection.image.fluid}
              title={contentSection.image.title}
              alt={contentSection.image.description}
            />
          ) : (
            <Swiper {...params}>
              {contentSection.imageGallery.map(i => (
                <Img
                  key={i.title}
                  fluid={i.fluid}
                  title={i.title}
                  alt={i.description}
                />
              ))}
            </Swiper>
          )}
        </Two>
      </Inner>
    </Wrapper>
  );
};

export default BoxTop;
