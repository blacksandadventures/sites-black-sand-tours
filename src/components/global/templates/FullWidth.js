import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  z-index: 1;
  > div {
    &:first-child {
      margin-bottom: 30px;
      position: relative;
      &:before {
        @media screen and (min-width: ${props =>
            props.theme.responsive.small}) {
          content: '';
          position: absolute;
          top: 26px;
          right: 14px;
          bottom: -54px;
          left: 14px;
          z-index: -1;
          background-color: ${props => props.theme.palette.background.middle};
        }
        @media screen and (max-width: ${props =>
            props.theme.responsive.medium}) and (min-width: ${props =>
            props.theme.responsive.small}) {
          right: 0px;
          left: 0px;
        }
      }
    }
  }
  &.is-slider {
    > div {
      &:before {
        @media screen and (min-width: ${props =>
            props.theme.responsive.small}) {
          content: '';
          position: absolute;
          top: 26px;
          right: 14px;
          bottom: -54px;
          left: 14px;
          z-index: -1;
          background-color: ${props => props.theme.palette.background.light};
        }
        @media screen and (max-width: ${props =>
            props.theme.responsive.medium}) and (min-width: ${props =>
            props.theme.responsive.small}) {
          right: 0px;
          left: 0px;
        }
      }
    }
  }
`;

const FullWidth = ({ children, isSlider }) => (
  <Wrapper className={isSlider ? 'is-slider' : ''}>{children}</Wrapper>
);

export default FullWidth;
