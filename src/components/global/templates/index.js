import Container from './Container';
import InnerContainer from './InnerContainer';
import PageContainer from './PageContainer';
import Box from './Box';
import FullWidth from './FullWidth';
import BoxInner from './BoxInner';
import PageTitle from './PageTitle';
import BoxTop from './BoxTop';
import BoxBottom from './BoxBottom';
import Loading from './Loading';
import SeoDetails from './SeoDetails';

export {
  Container,
  InnerContainer,
  PageContainer,
  Box,
  FullWidth,
  BoxInner,
  PageTitle,
  BoxTop,
  BoxBottom,
  Loading,
  SeoDetails
};
