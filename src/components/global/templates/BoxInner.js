import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  max-width: 1080px;
  margin-left: auto;
  margin-right: auto;
  box-sizing: content-box;
  padding-right: 42px;
  padding-left: 42px;
  z-index: 1;
  position: relative;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding-right: 21px;
    padding-left: 21px;
  }
  &.dark,
  &.primary,
  &.grey {
    padding: 100px;
    @media screen and (max-width: ${props =>
        props.theme.responsive.medium}) and (min-width: ${props =>
        props.theme.responsive.small}) {
      padding: 42px;
    }
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      padding: 35px 20px;
    }
    &:after {
      clear: both;
      content: '';
      display: table;
    }
  }
`;

const BoxInner = props => {
  const { Dark, Primary, Grey, children } = props;
  return (
    <Wrapper
      className={`${Dark ? 'dark' : ''} ${Primary ? 'primary' : ''} ${
        Grey ? 'grey' : ''
      }`}
    >
      {children}
    </Wrapper>
  );
};

export default BoxInner;
