import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

const Wrapper = styled.ul`
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    float: right;
  }
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    float: right;
    text-align: center;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    margin-right: 0px;
  }
`;

const Item = styled.li`
  display: inline-block;
  &:not(:last-child) {
    margin-right: 1em;
  }
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    text-align: center;
    margin-right: 0px;
    padding: 0 21px;
    &:not(:last-child) {
      margin-right: 0;
    }
  }
`;

const MenuLink = styled(Link)`
  font-weight: 400;
  font-size: 0.75em;
  line-height: 1em;
  display: inline-block;
  position: relative;
  color: ${props => props.theme.palette.text.grey};
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    font-size: 0.9em;
  }
`;

const WebsiteBy = styled.a`
  font-weight: 400;
  font-size: 0.75em;
  line-height: 1em;
  display: inline-block;
  position: relative;
  color: ${props => props.theme.palette.text.grey};
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    font-size: 0.9em;
  }
`;

const FooterLegal = () => (
  <StaticQuery
    query={graphql`
      query {
        contentfulMenus(id: { eq: "437431f2-c91e-5f42-97e3-79e6c76b0695" }) {
          id
          page {
            id
            menuTitle
            seoContent {
              pageTitle
              slug
            }
          }
        }
      }
    `}
    render={data => {
      const { page } = data.contentfulMenus;
      return (
        <Wrapper>
          {page.map(i => (
            <Item key={i.id}>
              <MenuLink
                to={`${i.seoContent.slug}/`}
                title={i.seoContent.pageTitle}
              >
                {i.menuTitle}
              </MenuLink>
            </Item>
          ))}
          <Item>
            <WebsiteBy
              href="https://discovrbookings.com"
              title="Tour internet marketing and website by Discovr Bookings"
              target="_blank"
              rel="noopener noreferrer"
            >
              Made by Discovr
            </WebsiteBy>
          </Item>
        </Wrapper>
      );
    }}
  />
);

export default FooterLegal;
