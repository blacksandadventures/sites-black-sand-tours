import styled from 'styled-components';

const FooterTopInner = styled.div`
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    max-width: 1080px;
    margin-left: auto;
    margin-right: auto;
    box-sizing: content-box;
    padding-right: 42px;
    padding-left: 42px;
  }
  @media screen and (max-width: ${props =>
      props.theme.responsive.medium}) and (min-width: ${props =>
      props.theme.responsive.small}) {
    display: table;
    margin: 0px auto;
  }
  &:after {
    clear: both;
    content: '';
    display: table;
  }
`;

export default FooterTopInner;
