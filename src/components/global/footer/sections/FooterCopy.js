import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';
import moment from 'moment';

import tripAdvisorAward from '../../../../assets/images/awards/tripadvisoraward.png';

const Wrapper = styled.ul`
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    float: left;
  }
`;

const Item = styled.li`
  display: block;
  font-size: 12px;
  &:first-child {
    margin-bottom: 20px;
  }
`;

const FooterCopy = () => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          companyName
          address
          city
          state
          postcode
          country
          email
          phone
        }
      }
    `}
    render={data => {
      const {
        companyName,
        address,
        city,
        state,
        postcode,
        country,
        email,
        phone
      } = data.contentfulCompanyInformation;
      return (
        <Wrapper>
          <Item>
            <img
              src={tripAdvisorAward}
              alt="trip advisor 2018 certificate of excellence"
            />
          </Item>
          <Item>
            <strong> &#x24B8;&nbsp;{companyName}&nbsp;{moment().year()}&nbsp;</strong>
            {address},&nbsp;{city}.&nbsp;{state}&nbsp;{postcode}.&nbsp;{country}
            .
          </Item>
          <Item>
            Phone: <a href={`tel:${phone}`}>{phone}</a>&nbsp; Email:{' '}
            <a href={`mailto:${email}`}>{email}</a>
          </Item>
        </Wrapper>
      );
    }}
  />
);

export default FooterCopy;
