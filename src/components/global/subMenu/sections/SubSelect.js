import React, { Component } from 'react';
import { navigate } from 'gatsby';
import styled from 'styled-components';

import IconDown from '../../../../assets/images/ui/icon-dd.png';

const Wrapper = styled.select`
  display: none;
  outline: none;
  margin-bottom: 0px;
  max-width: 100%;
  width: 100%;
  background-image: url(${IconDown});
  background-size: 15px 9px;
  padding-right: 4em;
  border-radius: 0px;
  background-position: right 1em center;
  background-repeat: no-repeat;
  background-color: ${props => props.theme.palette.background.light};
  box-shadow: none;
  box-sizing: border-box;
  vertical-align: top;
  -webkit-appearance: none;
  border-width: 1px;
  border-style: solid;
  border-color: ${props => props.theme.palette.border.dark};
  border-image: initial;
  margin: 0px;
  padding: 1.47727em;
  transition: border-color 150ms ease 0s;
  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    display: block;
    font-family: ${props => props.theme.font.header};
    font-size: 0.875em;
    letter-spacing: 0.15em;
    text-transform: uppercase;
    color: ${props => props.theme.palette.text.primary};
  }
`;

class SubSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = event => {
    const targetValue = event.target.value;
    navigate(targetValue);
  };

  render() {
    const { menu } = this.props;
    return (
      <Wrapper onChange={this.handleChange}>
        <option value="">Menu</option>
        {menu.page.map(i => (
          <option key={i.id} value={`${i.seoContent.slug}/`}>
            {i.seoContent.pageTitle}
          </option>
        ))}
      </Wrapper>
    );
  }
}

export default SubSelect;
