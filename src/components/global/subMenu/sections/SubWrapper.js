import styled from 'styled-components';

const SubWrapper = styled.section`
  background-color: ${props => props.theme.palette.background.light};
  border-bottom: 1px solid ${props => props.theme.palette.border.dark};
  position: sticky;
  right: 0;
  left: 0;
  z-index: 9;
  top: 71px;
  @media screen and (min-width: 48em) and (max-width: 73.6875em) {
    border-top: 1px solid ${props => props.theme.palette.border.dark};
    top: 55px;
  }
  @media screen and (max-width: 47.9375em) {
    border-top: 1px solid ${props => props.theme.palette.border.dark};
    top: 55px;
  }
  & > div {
    padding: 0.8em 21px !important;
  }
`;

export default SubWrapper;
