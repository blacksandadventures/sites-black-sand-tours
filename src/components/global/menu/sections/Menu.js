import React from 'react';
import { Link, StaticQuery, graphql } from 'gatsby';
import styled from 'styled-components';

const Wrapper = styled.ul`
  opacity: 0;
  transition: opacity 0s ease 0s, all 0.2s ease 0s;
  &.active {
    opacity: 1;
  }
`;

const Item = styled.li`
  opacity: 0;
  transition: opacity 400ms ease-in-out;
  transition-delay: 250ms;
  &.active {
    &:nth-child(1) {
      animation: fadeInUp 0.4s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.25s 1
        normal forwards running;
    }
    &:nth-child(2) {
      animation: fadeInUp 0.4s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.3s 1
        normal forwards running;
    }
    &:nth-child(3) {
      animation: fadeInUp 0.4s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.35s 1
        normal forwards running;
    }
    &:nth-child(4) {
      animation: fadeInUp 0.4s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.4s 1
        normal forwards running;
    }
    &:nth-child(5) {
      animation: fadeInUp 0.4s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.45s 1
        normal forwards running;
    }
  }
`;

const MenuLink = styled(Link)`
  font-family: ${props => props.theme.font.header};
  text-transform: uppercase;
  font-size: 1.25em;
  line-height: 2em;
  display: block;
`;

const Menu = props => (
  <StaticQuery
    query={graphql`
      query {
        contentfulMenus(id: { eq: "18891667-3616-5a45-854e-b22c22c3daf2" }) {
          id
          page {
            id
            seoContent {
              pageTitle
              slug
            }
          }
        }
      }
    `}
    render={data => {
      const { page } = data.contentfulMenus;
      const { menuOpen } = props;
      return (
        <Wrapper className={menuOpen ? 'active' : ''}>
          {page.map(i => (
            <Item key={i.id} className={menuOpen ? 'active' : ''}>
              <MenuLink
                to={`${i.seoContent.slug}/`}
                title={i.seoContent.pageTitle}
              >
                {i.seoContent.pageTitle}
              </MenuLink>
            </Item>
          ))}
        </Wrapper>
      );
    }}
  />
);

export default Menu;
