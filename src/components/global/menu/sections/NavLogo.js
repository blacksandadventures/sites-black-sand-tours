import React from 'react';
import { Link } from 'gatsby';
import styled from 'styled-components';

import Logo from '../../../../assets/images/logos/black-sand-logo-dark.svg';
import * as ROUTES from '../../../../constants/routes';
import config from '../../../../utils/siteConfig';

const Wrapper = styled(Link)`
  display: block;
  margin-left: 0px;
  text-indent: 101%;
  white-space: nowrap;
  background-size: 100%;
  height: 137px;
  width: 192px;
  background-image: url(${Logo});
  overflow: hidden;
  background-repeat: no-repeat;
  background-position: center top;
`;

const NavLogo = () => (
  <Wrapper to={ROUTES.INDEX} title={config.siteTitle}>
    {config.siteTitle}
  </Wrapper>
);

export default NavLogo;
