import React from 'react';
import {
  // Link,
  StaticQuery,
  graphql
} from 'gatsby';
import styled from 'styled-components';

// import * as ROUTES from '../../../../constants/routes';
import { ArrowRightIcon } from '../../../../assets/images/siteIcons';

const Wrapper = styled.ul`
  opacity: 0;
  margin: 1em 0px;
  transition: opacity 0s ease 0s, all 0.2s ease 0s;
  &.active {
    opacity: 1;
  }
`;

const Item = styled.li`
  opacity: 0;
  transition: opacity 400ms ease-in-out;
  transition-delay: 250ms;
  border-bottom: 1px solid ${props => props.theme.palette.border.dark};
  &:first-child {
    border-top: 1px solid ${props => props.theme.palette.border.dark};
  }
  &.active {
    &:nth-child(1) {
      animation: fadeInUp 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.5s 1
        normal forwards running;
    }
    &:nth-child(2) {
      animation: fadeInUp 0.3s cubic-bezier(0.455, 0.03, 0.515, 0.955) 0.55s 1
        normal forwards running;
    }
  }
`;

// const MenuLink = styled(Link)`
//   font-family: ${props => props.theme.font.header};
//   text-transform: uppercase;
//   font-size: 1.25em;
//   line-height: 2.3em;
//   display: block;
// `;

const MenuLinkExternal = styled.a`
  color: ${props => props.theme.palette.primary.main};
  font-family: ${props => props.theme.font.header};
  text-transform: uppercase;
  font-size: 1.25em;
  line-height: 2.3em;
  display: block;
  svg {
    width: 0.875em;
    margin: 0 10px -1px;
    transition: all 300ms ease-in-out;
  }
  &:hover {
    svg {
      transform: translate(10px, 0);
    }
  }
`;

const MenuFooter = props => (
  <StaticQuery
    query={graphql`
      query {
        contentfulCompanyInformation(
          id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
        ) {
          rezdyBookingLink
        }
      }
    `}
    render={data => {
      const { rezdyBookingLink } = data.contentfulCompanyInformation;
      const { menuOpen } = props;
      return (
        <Wrapper className={menuOpen ? 'active' : ''}>
          <Item className={menuOpen ? 'active' : ''}>
            <MenuLinkExternal
              href={rezdyBookingLink}
              title="Book your Black Sand Adventures Tour"
              target="_blank"
              rel="noopener noreferrer"
            >
              Book the tour
              <ArrowRightIcon />
            </MenuLinkExternal>
          </Item>
          {/* <Item className={menuOpen ? 'active' : ''}>
            <MenuLink to={ROUTES.MY_PHOTOS} title="Download your tour photos">
              My Photos
            </MenuLink>
          </Item> */}
        </Wrapper>
      );
    }}
  />
);

export default MenuFooter;
