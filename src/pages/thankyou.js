import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import Thanks from '../components/pages/thank-you';
import { ThanksWrapper } from '../components/pages/thank-you/sections';

const ThankYouPage = props => {
  const { data, location } = props;
  const { seoContent } = data.contentfulPages;

  return (
    <Layout whereToPage location={location} seoContent={seoContent}>
      <ThanksWrapper>
        <Hero thankYou location={location} seoContent={seoContent} />
        <Thanks />
      </ThanksWrapper>
    </Layout>
  );
};

export const query = graphql`
  query {
    contentfulPages(id: { eq: "96dbb420-dcad-5a2d-aa9d-f8853bb59a02" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 1800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
  }
`;

export default ThankYouPage;
