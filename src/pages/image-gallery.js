import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import { Box, BoxInner, PageTitle } from '../components/global/templates';
import GalleryTabs from '../components/pages/gallery';

class GalleryPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent } = data.contentfulPages;
    const menu = data.contentfulMenus;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box>
          <BoxInner>
            <PageTitle location={location} titleContent={seoContent} />
            <GalleryTabs />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "452c1f98-1661-5d92-8daf-d8c6ae64d7dc" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
    contentfulMenus(id: { eq: "0946fedb-57af-562f-b244-07b4eba3d482" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default GalleryPage;
