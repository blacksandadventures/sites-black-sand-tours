import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import {
  Box,
  BoxInner,
  BoxTop,
  FullWidth,
  BoxBottom
} from '../components/global/templates';
import Reviews from '../components/pages/reviews';
import {
  ReviewsTitle,
  NewsletterTitle,
  NewsletterBox,
  SocialWrapper
} from '../components/pages/home';

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent, contentSection } = data.contentfulPages;
    const { tripAdvisorId } = data.contentfulCompanyInformation;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero
          seoContent={seoContent}
          location={location}
          contentSection={contentSection[0]}
        />
        <Box TwoRows FirstBox>
          <FullWidth>
            <BoxTop reversed contentSection={contentSection[1]} />
          </FullWidth>
        </Box>
        <Box>
          <BoxInner>
            <NewsletterTitle />
            <NewsletterBox contentSection={contentSection} />
          </BoxInner>
        </Box>
        <Box Dark>
          <BoxInner Dark>
            <SocialWrapper contentSection={contentSection[4]} />
          </BoxInner>
        </Box>
        <Box>
          <BoxInner>
            <ReviewsTitle />
            <Reviews tripAdvisorId={tripAdvisorId} />
          </BoxInner>
        </Box>
        <Box TwoRows>
          <FullWidth>
            <BoxTop contentSection={contentSection[5]} />
            <BoxBottom contentSection={contentSection[6]} />
          </FullWidth>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "53bfebc1-61a1-5fbe-bf42-bdbd02055db7" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 1800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      contentSection {
        name
        bodyType {
          bodyType
          childMarkdownRemark {
            html
          }
        }
        image {
          title
          description
          fluid(maxWidth: 800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
        imageGallery {
          id
          title
          description
          file {
            url
          }
          fluid(maxWidth: 800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
        link {
          seoContent {
            pageTitle
            slug
          }
        }
      }
    }
    contentfulCompanyInformation(
      id: { eq: "0f9aa8b2-cc60-5fcf-ae40-3c4637824d7e" }
    ) {
      tripAdvisorId
    }
  }
`;

export default IndexPage;
