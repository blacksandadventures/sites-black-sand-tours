import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import Hero from '../components/global/hero';
import SubMenu from '../components/global/subMenu';
import {
  Box,
  BoxTop,
  FullWidth,
  BoxBottom,
  BoxInner
} from '../components/global/templates';
import { GoodToKnowTitle, GoodToKnowItems } from '../components/pages/about-us';

class AboutPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { data, location } = this.props;
    const { seoContent, contentSection } = data.contentfulPages;
    const menu = data.contentfulMenus;
    return (
      <Layout location={location} seoContent={seoContent}>
        <Hero location={location} seoContent={seoContent} />
        <SubMenu menu={menu} />
        <Box TwoRows FirstBox>
          <FullWidth>
            <BoxTop reversed contentSection={contentSection[0]} />
            <BoxBottom reversed contentSection={contentSection[1]} />
          </FullWidth>
        </Box>
        <Box>
          <BoxInner>
            <GoodToKnowTitle />
            <GoodToKnowItems contentSection={contentSection} />
          </BoxInner>
        </Box>
      </Layout>
    );
  }
}

export const query = graphql`
  query {
    contentfulPages(id: { eq: "17e2fd05-eaa7-5eea-819d-fc5cc5b61d4d" }) {
      id
      title
      seoContent {
        pageTitle
        slug
        description {
          description
        }
        featuredImage {
          title
          description
          fluid(maxWidth: 2000) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      contentSection {
        name
        bodyType {
          bodyType
          childMarkdownRemark {
            html
          }
        }
        image {
          title
          description
          fluid(maxWidth: 800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
        link {
          seoContent {
            pageTitle
            slug
          }
        }
      }
    }
    contentfulMenus(id: { eq: "0946fedb-57af-562f-b244-07b4eba3d482" }) {
      id
      page {
        id
        seoContent {
          pageTitle
          slug
        }
      }
    }
  }
`;

export default AboutPage;
