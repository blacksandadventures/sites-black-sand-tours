module.exports = {
  siteMetadata: {
    siteUrl: `https://www.blacksandtours.com`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-transformer-remark`,
    `gatsby-plugin-force-trailing-slashes`,
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: `/black-sands-sitemap.xml`,
        query: `
            {
              site {
                siteMetadata {
                  siteUrl
                }
              }
              allSitePage {
                edges {
                  node {
                    path
                  }
                }
              }
          }`
      }
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: `#55bbc0`,
        showSpinner: true
      }
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: 'https://www.blacksandtours.com'
      }
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: '33qnftts6ooc',
        accessToken:
          'e2f03c14ca761ecff73379578e0ec8111e8834555f0c11e4431d77ca979214f4'
      }
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Caveat', 'Comfortaa', 'Nunito']
        }
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Black Sand Adventures`,
        short_name: `Black Sand`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#55bbc0`,
        display: `standalone`,
        icon: `src/assets/images/icon.png`
      }
    },
    `gatsby-plugin-offline`
  ]
};
