const path = require(`path`);

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;

  const loadPagesLocations = new Promise(resolve => {
    graphql(`
      {
        allContentfulTourLocations {
          edges {
            node {
              id
              seoContent {
                slug
                pageTitle
              }
            }
          }
        }
      }
    `).then(result => {
      const location = result.data.allContentfulTourLocations.edges;
      location.forEach((edge, i) => {
        const prev = i === 0 ? null : location[i - 1].node;
        const next =
          i === location.length - 1 ? location[0].node : location[i + 1].node;
        createPage({
          path: `/where-we-go/${edge.node.seoContent.slug}/`,
          component: path.resolve(`./src/templates/pageLocation.js`),
          context: {
            slug: edge.node.seoContent.slug,
            prev,
            next
          }
        });
      });
      resolve();
    });
  });

  return Promise.all([loadPagesLocations]);
};
